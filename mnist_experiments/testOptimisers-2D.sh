#!/usr/bin/env bash
#------------------------------------------------------------------------------#
# Gaussian Processes based OPtimiser (GPOP) EXPERIMENTS in 2D (NN)             #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Run optimiser experiments on actual neural network given by     #
# configuration file, while optimising TWO hyper-parameters.                   #
#------------------------------------------------------------------------------#

# TO BE SET: Experiment settings
EXPERIMENT_CLASS="${EXPERIMENT_CLASS:-optimisers}"
#OPTIMISERS="${OPTIMISERS:-DEBUG}"
OPTIMISERS="${OPTIMISERS:-grid random gp gpmle}"
SPECIALIZATION="${SPECIALIZATION:-basic}"
NUM_EXPERIMENTS="${NUM_EXPERIMENTS:-5}"

# TO BE SET: GP optimiser settings
ROUNDS="${ROUNDS:-15}"
KERNEL="${KERNEL:-KernelMaternARD52}"
SAMPLES="${SAMPLES:-20}"
DSS="${DSS:-random}"
AF="${AF:-ExpectedImprovement}"
R="${R:-0.001}"
PARAMS="${PARAMS:-1.0 0.1 48}"

# TO BE SET: HYPER-PARAMETER & NN SETTINGS
CONFIG_FILE="${CONFIG_FILE:-./config_files/mnist_2D_lr_nb_hidden.txt}"

# initialisation
NUM_FILES=0
EXPERIMENT_SUBCLASS="${SPECIALIZATION}_kern-${KERNEL}_dss-${DSS}_af-${AF}"
DIM_TEXT="2D"


# run Grid optimiser
function run_grid_optimiser {
    ../gpop/wrapper.py \
    --rounds $ROUNDS \
    --grid-search \
    --seed $SEED \
    --config-file $CONFIG_FILE | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
}

# run Random optimiser
function run_random_optimiser {
    ../gpop/wrapper.py \
    --rounds $ROUNDS \
    --random-search \
    --seed $SEED \
    --config-file $CONFIG_FILE | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
}

# run GP optimiser without autotune
function run_gp_optimiser_wo_autotune {
    ../gpop/wrapper.py \
    --rounds $ROUNDS \
    --gaussian-process $KERNEL $SAMPLES $R $PARAMS \
    --domain-search-strategy $DSS \
    --acquisition-function $AF \
    --seed $SEED \
    --config-file $CONFIG_FILE | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
}

# run GP optimiser with autotune
function run_gp_optimiser_w_autotune {
    ../gpop/wrapper.py \
    --rounds $ROUNDS \
    --gaussian-process $KERNEL $SAMPLES $R $PARAMS \
    --domain-search-strategy $DSS \
    --acquisition-function $AF \
    --autotune \
    --seed $SEED \
    --config-file $CONFIG_FILE | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
}

# DEBUG: output what will be run
function run_debug {
    echo "----- OPTIMISER: $OPTIMISER -----" | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
    echo "../gpop/wrapper.py" | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
    echo "--rounds $ROUNDS" | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
    echo "--gaussian-process $KERNEL $SAMPLES $R $PARAMS" | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
    echo "--domain-search-strategy $DSS" | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
    echo "--acquisition-function $AF" | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
    echo "--autotune" | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
    echo "--seed $SEED" | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
    echo "--config-file $CONFIG_FILE | tee -a ${OUTPUT_FOLDER}/exp_${i}.log" | tee -a ${OUTPUT_FOLDER}/exp_${i}.log
}

# run proper optimiser
function run_optimiser {
    if [ ${OPTIMISER} == "grid" ]; then
        run_grid_optimiser
    elif [ ${OPTIMISER} == "random" ]; then
        run_random_optimiser
    elif [ ${OPTIMISER} == "gp" ]; then
        run_gp_optimiser_wo_autotune
    elif [ ${OPTIMISER} == "gpmle" ]; then
        run_gp_optimiser_w_autotune
    elif [ ${OPTIMISER} == "DEBUG" ]; then
        run_debug
    else
        echo "Something went wrong!"
        exit 1
    fi
}

# Log experiment settings.
function log_experiment_settings {
    timestamp=`date`
    echo "---------- $timestamp ----------"
    echo "Experiment settings:"
    echo " * EXPERIMENT_CLASS=$EXPERIMENT_CLASS"
    echo " * EXPERIMENT_SUBCLASS=${EXPERIMENT_SUBCLASS}"
    echo " * OPTIMISERS=${OPTIMISERS}"
    echo " * DIMENSIONS=${DIM_TEXT}"
    echo "GP settings:"
    echo " * ROUNDS=${ROUNDS}"
    echo " * KERNEL=${KERNEL}"
    echo " * SAMPLES=${SAMPLES}"
    echo " * DSS=${DSS}"
    echo " * AF=${AF}"
    echo " * R=${R}"
    echo " * PARAMS=${PARAMS}"
    echo "Configuration file '${CONFIG_FILE}':"
    cat ${CONFIG_FILE}
}

# Log run settings.
function log_run_settings {
    echo "[INFO] Running experiment with optimiser '$OPTIMISER' and neural network from configuration file '$CONFIG_FILE'..."
}

# Collect cumulative minumim results in OUTPUT_FOLDER.
function collect_cm_results {
    # for each line in file
    echo "[INFO] Collecting results in $OUTPUT_FOLDER..."
    for row in `seq 1 ${ROUNDS}`; do
        # for each file
        sum=0
        unset max
        unset min
        for file in ${OUTPUT_FOLDER}/*.log; do
            # save sum(average), max, min
            num=`sed -n "${row}p" $file`
            sum=`echo "$sum + $num" | bc -l`
            if [ -z "$max" ] || (( $(echo "$num > $max" |bc -l) )); then
                max=$num
            fi
            if [ -z "$min" ] || (( $(echo "$min > $num" |bc -l) )); then
                min=$num
            fi
        done
        # proper rounding
        average=`echo "scale=10; $sum / $NUM_FILES" | bc -l`

        # save results to files
        echo "  $row: [loss] average: $average, max: $max, min: $min"
        echo $average >> ${OUTPUT_FOLDER}/avg.txt
        echo $max >> ${OUTPUT_FOLDER}/max.txt
        echo $min >> ${OUTPUT_FOLDER}/min.txt
    done
}

# --RUN EXPERIMENTS--

for i in `seq 1 ${NUM_EXPERIMENTS}`; do # per each sector
    # create folder structure, log experiment settings
    mkdir -p ./data/${EXPERIMENT_CLASS}/${EXPERIMENT_SUBCLASS}
    log_experiment_settings | tee -a ./data/${EXPERIMENT_CLASS}/${EXPERIMENT_SUBCLASS}/log.txt

    # Select 'random' SEED for the run
    SEED=`echo $((1 + RANDOM % 999))`

    # run optimisation for all optimisers
    for OPTIMISER in ${OPTIMISERS}; do
        EXPERIMENT_NAME="${OPTIMISER}_${DIM_TEXT}_r${ROUNDS}s${SAMPLES}"
        OUTPUT_FOLDER="./data/${EXPERIMENT_CLASS}/${EXPERIMENT_SUBCLASS}/${EXPERIMENT_NAME}"
        mkdir -p $OUTPUT_FOLDER
        log_run_settings | tee -a ${OUTPUT_FOLDER}/log.txt
        run_optimiser
    done
    (( NUM_FILES = NUM_FILES + 1 ))
done

# --COLLECT RESULTS (cumulative minimum)--

for OPTIMISER in ${OPTIMISERS}; do
    EXPERIMENT_NAME="${OPTIMISER}_${DIM_TEXT}_r${ROUNDS}s${SAMPLES}"
    OUTPUT_FOLDER="./data/${EXPERIMENT_CLASS}/${EXPERIMENT_SUBCLASS}/${EXPERIMENT_NAME}"
    collect_cm_results
done

echo "[INFO] done"
