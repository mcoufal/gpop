#!/usr/bin/env bash

DATA_SET=default_seed
DATA_FOLDER=${DATA_SET}/data
OUTPUT_FILE=results_${DATA_SET}.txt
TMP_FILE=tmpfile.txt

rm -rf $OUTPUT_FILE
for i in `seq 1 100`; do
    filename=`ls ${DATA_FOLDER}/${i}-LR-* | cat`
    cat $filename | grep "Test set: Average loss:" | tail -1 | cut -d ":" -f 3 | cut -d "," -f 1 >> $TMP_FILE;
done

i=1
while IFS= read -r line; do
    if ! (($i % 4)); then
        echo "$line," >> $OUTPUT_FILE
    else
        echo -n "$line, " >> $OUTPUT_FILE
    fi
    (( ++i ))
done < $TMP_FILE

rm -rf $TMP_FILE
