import unittest
import numpy as np
import torch
from gpop.optimiser import kernel


class TestKernel(unittest.TestCase):

    def setUp(self):
        pass

    def test_kernelRBF_sanity_1D(self):
        # x1 == x2
        x1 = torch.autograd.Variable(torch.from_numpy(np.array([1.0])))
        x2 = torch.autograd.Variable(torch.from_numpy(np.array([1.0])))
        params = torch.autograd.Variable(torch.from_numpy(np.array([1.0])), requires_grad=True)
        k = kernel.selectKernel("KernelRBF", params)
        self.assertEqual(1.0, k(x1, x2).item()) # x1 == x2 -> k(x1, x2) == 1.0

        # x1 == x2 == 0
        x1 = torch.autograd.Variable(torch.from_numpy(np.array([0.0])))
        x2 = torch.autograd.Variable(torch.from_numpy(np.array([0.0])))
        params = torch.autograd.Variable(torch.from_numpy(np.array([1.0])), requires_grad=True)
        k = kernel.selectKernel("KernelRBF", params)
        self.assertEqual(1.0, k(x1, x2).item()) # x1 == x2 -> k(x1, x2) == 1.0

        # x1 == -1 & x2 == 2
        x1 = torch.autograd.Variable(torch.from_numpy(np.array([-1.0])))
        x2 = torch.autograd.Variable(torch.from_numpy(np.array([2.0])))
        params = torch.autograd.Variable(torch.from_numpy(np.array([1.0])), requires_grad=True)
        k = kernel.selectKernel("KernelRBF", params)
        self.assertEqual(0.011108996538242306, k(x1, x2).item()) # x1 == -1 & x2 == 2 -> k(x1, x2) == 0.011108996538242306

        # x1 == 2 & x2 == -1
        x1 = torch.autograd.Variable(torch.from_numpy(np.array([2.0])))
        x2 = torch.autograd.Variable(torch.from_numpy(np.array([-1.0])))
        params = torch.autograd.Variable(torch.from_numpy(np.array([1.0])), requires_grad=True)
        k = kernel.selectKernel("KernelRBF", params)
        self.assertEqual(0.011108996538242306, k(x1, x2).item()) # x1 == 2 & x2 == -1 -> k(x1, x2) == 0.011108996538242306

    def test_buildMultKernel(self):
        pass

    def test_buildAddKernel(self):
        pass

if __name__ == '__main__':
    unittest.main()
