import numpy as np
import torch

# Gaussian Process
# Given some points and kernel function, represents a Gaussian process G(mean,
# covMatrix) that models a probable curve that complies with given kernel.
# Based on code from muatik: https://github.com/muatik/machine-learning-examples/blob/master/gaussianprocess2.ipynb
# @x:      N-dimensional array of training points
# @y:      N-dimensional array of measured values in training points x
# @kernel: kernel function k(x, y): R ^ N x R ^ N -> R ^ N
# @R:      measurement uncertainty
class GP():

    # initialise GP
    def __init__(self, x, y, kernel, R=0):
        self.x = x # training points coordinates
        self.y = y # training points values
        self.N = len(self.x)
        self.R = R
        self.mean = []
        self.kernel = kernel
        self.covMatrix = self.__calculateCovarianceMatrix()

    # calculate covariance matrix based on GP's measuring points x, kernel function and
    def __calculateCovarianceMatrix(self):
        with torch.no_grad():
            sigma = np.ones((self.N, self.N))
            for i in range(self.N):
                for j in range(i, self.N):
                    cov = self.kernel(torch.from_numpy(self.x[i]), torch.from_numpy(self.x[j]))
                    sigma[i][j] = cov
                    sigma[j][i] = cov

            sigma = sigma + self.R.item() * np.eye(self.N)
            return sigma

    # predic value of test point X, based on created GP
    def predict(self, x):
        with torch.no_grad():
            cov = 1 + self.R.item() * self.kernel(torch.from_numpy(x), torch.from_numpy(x)).numpy()
            sigma_1_2 = np.zeros((self.N, 1))
            for i in range(self.N):
                sigma_1_2[i] = self.kernel(torch.from_numpy(self.x[i]), torch.from_numpy(x))

            # SIGMA_1_2 * SIGMA_1_1.I * (Y.T -M)
            # M IS ZERO
            try:
                m_expt = (sigma_1_2.T * np.mat(self.covMatrix).I) * np.mat(self.y).T
            except: # @FIXME: issue #1
                for row in self.covMatrix:
                    print("")
                    for item in row:
                        print("{} ".format(item), end = '')
                print("")

            # sigma_expt = cov - (sigma_1_2.T * np.mat(self.covMatrix).I) * sigma_1_2
            sigma_expt = cov + self.R.item() - (sigma_1_2.T * np.mat(self.covMatrix).I) * sigma_1_2
            return m_expt, sigma_expt
