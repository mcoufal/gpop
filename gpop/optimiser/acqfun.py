#------------------------------------------------------------------------------#
# GPOP - Acquisition functions                                                 #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Library of acquisition functions used by GP optimiser to select #
# next training point for black-box optimisation.                              #
#------------------------------------------------------------------------------#

import numpy as np
from scipy.stats import norm


# Returns callable acquisition function based on its name. Don't forget to update
# this dictionary with every new implemented acquisition functions in this file.
def selectAcqFun(name):
    return {
        'MinimalMean': MinimalMean(),
        'LowerConfidence': LowerConfidence(),
        'ExpectedImprovement': ExpectedImprovement()
    }[name]


# Acquisition function parent class
class AcquisitionFunction:

    # Callable acquisition function template.
    def __call__(self, x_guess, gp, x, y):
        raise NotImplementedError("The base class is not callable!")


# Sequential Design Optimisation function given by mean only
class MinimalMean(AcquisitionFunction):

    # Find X index of minimal value in mean prediction @y.
    def __findNextXValue(self, y):
        val, xindex = min((val, xindex) for (xindex, val) in enumerate(y))
        return xindex

    # Returns the point with the lowest mean.
    def __call__(self, x_guess, gp, x, y):
        # make predictions
        mean_pred_next = []
        sigma_pred_next = []
        x_guess_count = len(x_guess)
        for i in range(x_guess_count):
            res = gp.predict(x_guess[i])
            mean_pred_next.append(res[0].item(0))
            sigma_pred_next.append(res[1].item(0))
        # save preditions, so it's possible to call optimiser round by round
        mean_pred = mean_pred_next
        sigma_pred = sigma_pred_next

        # find prediction with minimal mean value
        xindex = self.__findNextXValue(mean_pred)
        x_next = x_guess[xindex]

        return x_next, mean_pred, sigma_pred


# Sequential Design Optimisation function given by Lower Confidence Bound
class LowerConfidence(AcquisitionFunction):

    # Find X index of minimal value given by: mean - 2 * sqrt(sigma).
    def __findNextXValue(self, mean, sigma):
        min = mean[0] - 2 * np.sqrt(sigma[0])
        min_index = 0
        for (xindex, val) in enumerate(mean):
            min_uncert = val -  2 * np.sqrt(sigma[xindex])
            if (min_uncert < min):
                min = min_uncert
                min_index = xindex

        return min_index

    # Returns the point with the lowest confidence bound.
    def __call__(self, x_guess, gp, x, y):
        # make predictions
        mean_pred = []
        sigma_pred = []
        x_guess_count = len(x_guess)
        for i in range(x_guess_count):
            res = gp.predict(x_guess[i])
            mean_pred.append(res[0].item(0))
            sigma_pred.append(res[1].item(0))
        # find prediction with minimal mean value
        xindex = self.__findNextXValue(mean_pred, sigma_pred)
        x_next = x_guess[xindex]

        return x_next, mean_pred, sigma_pred


# Expected Improvement Acquisition function
class ExpectedImprovement(AcquisitionFunction):

    def __z(self, imp, sigma_x):
        z = []
        for i in range(len(imp)):
            if (sigma_x[i] == 0):
                z.append(0)
            else:
                z.append(imp[i] / sigma_x[i])
        return np.array(z)

    # Returns the point with the best Expected Improvement.
    def __call__(self, x_guess, gp, x, y, xi=0.01):
        # make predictions
        mean_x_guess = []
        sigma_x_guess = []
        x_guess_count = len(x_guess)
        for i in range(x_guess_count):
            res = gp.predict(x_guess[i])
            mean_x_guess.append(res[0].item(0))
            sigma_x_guess.append(res[1].item(0))

        # calculate expected improvement
        f_xp = np.min(y)
        imp = mean_x_guess - f_xp - xi
        z = self.__z(imp, sigma_x_guess)
        ei = imp * norm.cdf(z) + sigma_x_guess * norm.pdf(z)

        # select best point X
        val, xindex = min((val, xindex) for (xindex, val) in enumerate(ei))
        x_next = x_guess[xindex]

        return x_next, mean_x_guess, sigma_x_guess
