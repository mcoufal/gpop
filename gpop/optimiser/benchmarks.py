#------------------------------------------------------------------------------#
# GPOP - Benchmarks                                                            #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Library of reference functions used by optimiser as black-box   #
# functions for optimisation. Apart from simple example functions, contains    #
# widely used functions used in optimiser performance testing. Each benchmark  #
# function is implemented as callable class with additional methods providing  #
# more information about specific benchmark.                                   #
#------------------------------------------------------------------------------#
import numpy as np
import random


# Return benchmark function based on its name. Don't forget to update this
# dictionary as soon as implemeented benchmarks in this file.
def selectBenchmark(name, x_opt, f_opt, jitter=None):
    return {
        # simple dimension specific benchmarks
        'FnQuadratic': FnQuadratic(x_opt, f_opt, jitter=jitter),
        'FnCubic': FnCubic(x_opt, f_opt, jitter=jitter),
        'FnQuadraticWithSin': FnQuadraticWithSin(x_opt, f_opt, jitter=jitter),
        'FnQuadratic2D': FnQuadratic2D(x_opt, f_opt, jitter=jitter),
        'FnQuadratic2Dx': FnQuadratic2Dx(x_opt, f_opt, jitter=jitter),
        'FnQuadratic3D': FnQuadratic3D(x_opt, f_opt, jitter=jitter),
        # benchmarks from: "Real-Parameter Black-Box Optimization Benchmarking"
        'FnSphere': FnSphere(x_opt, f_opt, jitter=jitter),
        'FnEllipsoidal': FnEllipsoidal(x_opt, f_opt, jitter=jitter)
    }[name]

# Base class for all benchmark functions. In case some benchmark does not
# implement used method, 'NotImplementedError' is thrown.
class Benchmark:

    # Initialise benchmark with known optimum and optimal value.
    def __init__(self, x_opt, f_opt, jitter=None):
        self.x_opt = x_opt
        self.f_opt = f_opt
        self.jitter = jitter

    # Callable benchmark function. Takes one argument, which is an array
    # representing hyper-parameter settings (point coordinates)
    def __call__(self, x):
        raise NotImplementedError("The benchmark method is not implemented!")

    # Returns string representation of the benchmark function.
    def __getStringRepresentation__(self):
        raise NotImplementedError("The string representation of benchmark is not implemented!")

    # Returns an array representing optimal benchmark function input.
    def __getOptimum__(self):
        return self.x_opt

    # Returns optimal benchmark function value.
    def __getOptimalFunctionValue__(self):
        return self.f_opt


# y = (x - 2)^2
class FnQuadratic(Benchmark):

    def __call__(self, x):
        return np.power(x - 2, 2)

    def __getStringRepresentation__(self):
        return "y = (x - 2)^2"


# y = (x - 2)^3 + 5
class FnCubic(Benchmark):

    def __call__(self, x):
        return np.power(x - 2, 3) + 5

    def __getStringRepresentation__(self):
        return "y = (x - 2)^3 + 5"


# y = (x - 2)^2 + sin(2x + 3)
class FnQuadraticWithSin(Benchmark):

    def __call__(self, x):
        return np.power(x - 2, 2) + np.sin(2 * x + 3)

    def __getStringRepresentation__(self):
        return "y = (x - 2)^2 + sin(2x + 3)"


# y = (x - 2)^2 + y^2 + 2
class FnQuadratic2D(Benchmark):

    def __call__(self, x):
        return np.power(x[0] - 2, 2) + np.power(x[1], 2) + 2

    def __getStringRepresentation__(self):
        return "y = (x - 2)^2 + y^2 + 2"


# y = x + [-2, 1] * [[4.0, 2.0], [2.0, 1.0]] * (x + [-2, 1]).T
class FnQuadratic2Dx(Benchmark):

    def __call__(self, x):
        A = np.asarray([[4.0, 2.0], [2.0, 1.0]]) # won't work in multiple-dim
        x_tmp = x + np.asarray([-2, -1])
        return x_tmp @ A @ x_tmp.T + 2

    def __getStringRepresentation__(self):
        return "x + [-2, 1] * [[4.0, 2.0], [2.0, 1.0]] * (x + [-2, 1]).T"


# y = x^2 + y^2 + z^2 + 2
class FnQuadratic3D(Benchmark):

    def __call__(self, x):
        return np.power(x[0], 2) + np.power(x[1], 2) + np.power(x[2], 2) + 2

    def __getStringRepresentation__(self):
        return "y = x^2 + y^2 + z^2 + 2"


#------------------------------------------------------------------------------#
# BENCHMARKS from: Real-Parameter Black-Box Optimization Benchmarking          #
#                  2009: Presentation of the Noiseless Functions               #
# coco.gforge.inria.fr/lib/exe/fetch.php?media=download3.6:bbobdocfunctions.pdf#
#------------------------------------------------------------------------------#

def t_osz(x):
    x_hat = np.log(np.abs(x)) if not x == 0 else 0
    c_1 = 10 if x > 0 else 5.5
    c_2 = 7.9 if x > 0 else 3.1
    return sign(x) * np.exp(x_hat + 0.049 * (np.sin(c_1 * x_hat) + np.sin(c_2 * x_hat)))

def sign(x):
    if x < 0:
        return -1
    elif x == 0:
        return 0
    else:
        return 1

# Separable functions

# Sphrere benchmark
class FnSphere(Benchmark):

    def __call__(self, x):
        if self.jitter is not None:
            jitter = random.uniform(-self.jitter, self.jitter)
        else:
            jitter = 0.0
        return np.power(np.linalg.norm(x - self.x_opt), 2) + self.f_opt + jitter

    def __getStringRepresentation__(self):
        return "y = ||z||^2 + f_opt, z = x - x_opt"


# Elipsoidal benchmark
class FnEllipsoidal(Benchmark):

    def __call__(self, x):
        if self.jitter is not None:
            jitter = random.uniform(-self.jitter, self.jitter)
        else:
            jitter = 0.0
        d = x.size
        sum = 0
        # 1D ellipsoidal function
        if (d == 1):
            z = t_osz(x[0] - self.x_opt[0])
            sum += z**2 + self.f_opt
        # n-D ellipsoidal functions
        else:
            for i in range(1, d):
                z = t_osz(x[i - 1] - self.x_opt[i - 1])
                sum += 10**(6 * (i - 1) / (d - 1)) * z**2 + self.f_opt
        return sum + jitter

    def __getStringRepresentation__(self):
        return "sum_i(10**(6 * (i - 1) / (d - 1)) * z_i**2 + f_opt)"
