import numpy as np
import torch


# Return callable kernel based on its name. Don't forget to update this
# dictionary as soon as implemented kernels in this file.
def selectKernel(name, params):
    if name == 'KernelRBF': return KernelRBF(params)
    elif name == 'KernelLaplacian': return KernelLaplacian(params)
    elif name == 'KernelMaternARD52': return KernelMaternARD52(params)
    elif name == 'KernelConstant': return KernelConstant(params)
    elif name == 'KernelLinear': return KernelLinear(params)


# Radial Basis Function Kernel
# Represents a RBF kernel with adjustable parameter delta.
class KernelRBF():

    def __init__(self, params):
        self.params = params
        self.delta = self.params[0]

    def __call__(self, x, y):
        d_sq = torch.sum((x - y)**2)
        res = torch.exp(-1/2.0 * d_sq / (self.delta**2))
        return res


# Laplacian Kernel
# Represents a Laplacian kernel with adjustable parameter delta.
class KernelLaplacian():

    def __init__(self, params):
        self.params = params
        self.delta = self.params[0]

    def __call__(self, x, y):
        d_abs = torch.sum(np.abs(x - y))
        res = torch.exp(-1/2.0 * d_abs / self.delta)
        return res


# Matern ARD Kernel 5/2
# Represents a Matern ARD kernel with adjustable parameters delta and variance
# as described here:
# https://www.mathworks.com/help/stats/kernel-covariance-function-options.html.
class KernelMaternARD52():

    def __init__(self, params):
        self.params = params
        self.sigma = self.params[0]
        self.delta = self.params[1:]

    def __call__(self, x, y):
        # autotune: apply square root only if not zero, since square root has no gradient at 0
        tmp = torch.sum(((x - y)**2) / (self.delta**2))
        if tmp.item() != 0:
            r = torch.sqrt(tmp)
        else:
            r = tmp
        res = (self.sigma**2) * (1 + np.sqrt(5) * r + 5/3.0 * r**2) * torch.exp(-np.sqrt(5) * r)
        return res


# Constant Kernel
# Represents a constant kernel with adjustable parameter c.
class KernelConstant():

    def __init__(self, c=1.0):
        self.c = c

    def __call__(self, x, y):
        return self.c


# Linear Kernel
# Represents a linear kernel with adjustable parameter theta.
class KernelLinear():

    def __init__(self, theta=1.0):
        self.theta = theta

    def __call__(self, x, y):
        return self.theta * x.T * y

# Build multiplicative kernel
def buildMultKernel(k1, k2):
    return lambda x1, x2: k1(x1, x2) * k2(x1, x2)

# Build additive kernel
def buildAddKernel(k1, k2):
    return lambda x1, x2: k1(x1, x2) + k2(x1, x2)
