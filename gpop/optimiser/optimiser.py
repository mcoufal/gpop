import random
import math
import numpy as np
import torch
import itertools
from gpop.optimiser import gp
from gpop.optimiser import acqfun
from gpop.optimiser import kernel
# DEBUG
from matplotlib import pyplot as plt
from matplotlib import cm

# Generate random point within dimension bounds @dim_bounds, where each value is of type @htypes.
def generateRandomPoint(dim_bounds, htypes):
    x_init = []
    for dim in range(len(dim_bounds)):
        if htypes is None or htypes[dim] == "float":
            x_init.append(random.uniform(dim_bounds[dim][0], dim_bounds[dim][1]))
        elif htypes[dim] == "int":
            x_init.append(random.randint(dim_bounds[dim][0], dim_bounds[dim][1]))
        # TODO: will be treated as integer, transition from CATEGORICAL -> INT will be handled in bridge.NeuralNet
        # but for now lets differentiate between the two, since this feature is not yet implemented!
        elif htypes[dim] == "cat":
            raise NotImplementedError("Categorical hyper-parameter values not implemented yet!")
        else:
            raise ValueError("Hyper-parameter type '{}' not valid!".format(htypes[i]))
    return np.array(x_init)

# DEBUG: Generate grid for plotting.
def generatePlotXCoordinates(dim_bounds, nb_samples):
    # create list of linear spaced arrays, each containing values of one dimension
    x_tmp = []
    for i in range(0,len(dim_bounds)):
        x_tmp.append(np.linspace(dim_bounds[i][0], dim_bounds[i][1], nb_samples))

    # create cartesian product of those arrays and convert it into matrix
    cartesian_tuples = [item for item in itertools.product(*x_tmp)]
    cartesian_list = [list(ele) for ele in cartesian_tuples]
    x_guess = np.array(cartesian_list)

    return x_guess.transpose()


# DEBUG: draw original function (1D parameter, 2D plot)
def drawOriginal2D(ax, fn, dim_bounds, nb_samples):
    x = np.linspace(dim_bounds[0][0], dim_bounds[0][1], nb_samples)
    y = []
    for i in range(len(x)):
        tmp = []
        tmp.append(x[i])
        y.append(fn(np.array(tmp)))
    ax.plot(x, y, linestyle='--')

# DEBUG: draw GP prediction of the function (1D parameter, 2D plot)
def drawPrediction2D(ax, x, fn, alpha, cmap, tr_x, tr_y, uncertainty=False):
    tmp_y = []
    tmp_pred = []
    tmp_x = x.T

    for i in range(0, len(tmp_x)):
        tmp = fn(tmp_x[i])
        next_y = (tmp[0]).item(0)
        tmp_y.append(next_y)
        next_pred = (tmp[1]).item(0)
        tmp_pred.append(next_pred)

    ax.set_facecolor('w')
    ax.plot(x[0], tmp_y, alpha=alpha)
    if uncertainty:
        ax.fill_between(x[0], tmp_y - 2 * np.sqrt(tmp_pred), tmp_y + 2 * np.sqrt(tmp_pred), color="#dddddd")

    # draw training poins
    for i in range(0, len(tr_x)):
        ax.scatter(tr_x[i], tr_y[i], c="k")


# Gaussian Processes Optimiser
class GPOptimiser():

    # initialise optimiser
    def __init__(self, fn, dim_bounds, kernel, x_init=None, R=1e-5, nb_samples=50, dss="random", acq_fun="ExpectedImprovement", htypes=None, autotune=False, autotune_all=False, verbose=False):
        self.fn = fn # instead of running NN and measuring the value
        self.x_init = generateRandomPoint(dim_bounds, htypes) if x_init is None else x_init
        self.dim_bounds = dim_bounds
        self.N = len(dim_bounds) # number of dimensions (same as len(x_init))
        self.x = [] # training points coordinates
        self.y = [] # training points values
        self.kernel = kernel # kernel function
        self.R = torch.autograd.Variable(torch.tensor(math.log(R)), requires_grad=True) # uncertainty
        self.gp = None # GP model
        self.nb_samples = nb_samples # number of samples per dimension
        self.non_optimised = True # for repeated optimise() calls
        self.mean_pred = None # save predictions for round by round optimisation
        self.sigma_pred = None # save predictions for round by round optimisation
        self.best_x = None # best found vector x*
        self.best_y = None # best vector's function value f(x*)
        self.dss = dss
        self.acq_fun = acqfun.selectAcqFun(acq_fun) # select acqusition function
        self.htypes= htypes # list of hyper-parameter value types (float, integer, categorical)
        self.verbose = verbose
        self.autotune = autotune # turn kernel parameter optimisation on/off
        self.autotune_all = autotune_all # turn kernel + uncertainty parameter optimisation on/off
        if autotune:
            self.mle = torch.optim.Adam([self.kernel.params], lr=1e-2)
        elif autotune_all:
            self.mle = torch.optim.Adam([self.kernel.params, self.R], lr=1e-2)
        else:
            self.mle = None
        self.mle_eps = 0.5 # for MLE stopping condition

    # add new measuring point, it's value and update GP accordingly
    def __updateGPModel(self, x, y):
        self.x.append(x)
        self.y.append(y)
        self.gp = gp.GP(self.x, self.y, self.kernel, self.R.exp())

    # Create a matrix where each row 'i' represents test X coordinate of i-th dimension
    def __generateGridTestCoordinates(self):
        # create list of linear spaced arrays, each containing values of one dimension
        x_tmp = []

        for i in range(0,self.N):
            if self.htypes is None or self.htypes[i] == "float":
                x_tmp.append(np.linspace(self.dim_bounds[i][0], self.dim_bounds[i][1], self.nb_samples))
            elif self.htypes[i] == "int":
                x_tmp.append(np.linspace(self.dim_bounds[i][0], self.dim_bounds[i][1], self.nb_samples, dtype = int))
            elif self.htypes[i] == "cat":
                raise NotImplementedError("Categorical hyper-parameter values not implemented yet!")
            else:
                raise ValueError("Hyper-parameter type '{}' not valid!".format(htypes[i]))

        # create cartesian product of those arrays and convert it into matrix
        cartesian_tuples = [item for item in itertools.product(*x_tmp)]
        cartesian_list = [list(ele) for ele in cartesian_tuples]
        x_guess = np.array(cartesian_list)
        # x_guess = [[x1,x2,x3,...] - 1st point (with dimensions x1, x2, x3, ...)
        #            [x1,x2,x3,...] - 2nd point
        #            .............] - n-th point
        return x_guess

    # Generate random testing points within @dim_bounds.
    def __generateRandomTestCoordinates(self):
        # create list of linear spaced arrays, each containing values of one dimension
        x_tmp = []

        for i in range(self.nb_samples ** self.N):
            x_tmp.append(generateRandomPoint(self.dim_bounds, self.htypes))

        return np.array(x_tmp)

    # Generate jitter.
    def __generateJitter(self, coef=10):
        jitter = []
        for i in range(0,self.N):
            dist = abs(self.dim_bounds[i][0] - self.dim_bounds[i][1])
            dim_jitter = dist/self.nb_samples/coef
            rand = random.uniform(-dim_jitter, dim_jitter)
            jitter.append(rand)

        return np.array(jitter)

    # Transform to proper variable types.
    def __transformToHparamTypes(self, x):
        for i in range(x.size):
            if self.htypes is None or self.htypes[i] == "float":
                continue
            elif self.htypes[i] == "int":
                x[i] = int(round(x[i]))
            # TODO: will be treated as integer, transition from CATEGORICAL -> INT will be handled in bridge.NeuralNet
            # but for now lets differentiate between the two, since this feature is not yet implemented!
            elif self.htypes[i] == "cat":
                raise NotImplementedError("Categorical hyper-parameter values not implemented yet!")
            else:
                raise ValueError("Hyper-parameter type '{}' not valid!".format(self.htypes[i]))

    # Calculate covariance matrix of GP, use Pytorch to enable autotune.
    def __calculateTensorCovarianceMatrix(self, x):
        N = len(self.x)
        sigma = torch.ones((N, N), dtype=torch.float64)
        for i in range(N):
            for j in range(i, N):
                cov = self.kernel(x[i], x[j])
                sigma[i][j] = cov
                sigma[j][i] = cov

        sigma = sigma + self.R.exp() * torch.eye(N)
        return sigma

    # Calculate log likelihood of GP.
    def __getLogLikelihood(self, cov):
        c_n = cov.double()
        t = torch.from_numpy(np.array(self.gp.y)[np.newaxis])
        n = len(self.gp.y)
        log_like = -1/2.0 * torch.log(torch.cholesky(c_n).diag().prod()) - 1/2.0 * t.mm(c_n.inverse()).mm(t.T) - n/2.0 * np.log(2 * math.pi)
        log_like.requires_grad_(True)
        return log_like

    # Automatic parameter tuning using MLE.
    def __tuneParams(self, rounds=50):
        x = torch.from_numpy(np.asmatrix(self.gp.x))
        llh_last = 0

        for i in range(rounds):
            self.mle.zero_grad()
            cov = self.__calculateTensorCovarianceMatrix(x)
            llh = self.__getLogLikelihood(cov)
            if self.verbose:
                print("[{}]: LLH: {}  |  PARAMS: {}  |  R: {}".format(i, llh.item(), self.kernel.params, self.R.exp()))

            # DEBUG: plot
            if False:
                jitter = self.fn.jitter
                self.fn.jitter = 0.0
                new_gp = gp.GP(self.x, self.y, self.kernel, self.R.exp())
                xx = generatePlotXCoordinates(self.dim_bounds, 100)
                fig = plt.figure()
                fig.suptitle("kparams: {} | R: {} | LLH: {}".format(self.kernel.params.detach().numpy(), self.R.exp(), llh.item()))
                ax = fig.add_subplot(111)
                ax.grid(color='0.75', linestyle='--', linewidth=1)
                drawPrediction2D(ax, xx, new_gp.predict, 0.8, cm.coolwarm, new_gp.x, new_gp.y, uncertainty=True)
                drawOriginal2D(ax, self.fn, self.dim_bounds, 500)
                filename = "MLE/R{}-r{}.png".format(len(self.y), i)
                fig.savefig(filename, bbox_inches='tight')
                self.fn.jitter = jitter

            (-llh).backward()
            self.mle.step()

            # stopping condition
            if i > 10 and (torch.abs(llh - llh_last) < self.mle_eps):
                break
            else:
                llh_last = llh

    # Optimise for @rounds rounds. Returns GP object.
    def optimise(self, rounds):
        # for each round of optimisation
        for i in range(rounds):
            # initialise if first round
            if (self.non_optimised):
                x_next = self.x_init
            # else make prediction
            else:
                # get test X coordinates for next prediction
                if (self.dss == "random"):
                    x_guess = self.__generateRandomTestCoordinates()
                else:
                    x_guess = self.__generateGridTestCoordinates() + self.__generateJitter() #FIXME: @# BUG: https://gitlab.com/mcoufal/gpop/issues/1
                # use acquisition function to get next training point X
                x_next, self.mean_pred, self.sigma_pred = self.acq_fun(x_guess, self.gp, self.x, self.y)
                # check values of x_next actualy updated
                self.__transformToHparamTypes(x_next)

            # calculate actual value and update model
            y_actual = self.fn(x_next)
            if (self.non_optimised):
                self.best_y = y_actual
                self.non_optimised = False
            if (y_actual <= self.best_y):
                self.best_x = x_next
                self.best_y = y_actual
            if self.verbose:
                print("[round {}] settings: {}, value: {}".format(i + 1, x_next, y_actual))
            self.__updateGPModel(x_next, y_actual)
            if (self.autotune or self.autotune_all) and i >= 5:
                self.__tuneParams()

        return self.gp


# Random Search Optimiser
class RandomSearchOptimiser():

    # initialise optimiser
    def __init__(self, fn, dim_bounds, verbose=False):
        self.fn = fn # instead of running NN and measuring the value
        self.dim_bounds = dim_bounds
        self.N = len(dim_bounds) # number of dimensions
        self.verbose = verbose

    # Randomly generate points within given dimension bounds and return the one
    # with the best result.
    def optimise(self, rounds):
        y_all = []
        x_all = []
        # For certain amount of rounds, generate random X coordinates and search for best function value
        for round in range(rounds):
            x_guess = []
            for dim in range(self.N):
                x_guess.append(random.uniform(self.dim_bounds[dim][0], self.dim_bounds[dim][1]))
            x_all.append(x_guess)
            val = self.fn(np.array(x_guess))
            y_all.append(val)
            if (round == 0) or (best_value > val):
                best_value = val
                x_best = x_guess
            if self.verbose:
                print("[round {}] settings: {}, value: {}".format(round + 1, x_guess, val))

        return np.array(x_all).transpose(), y_all, x_best, best_value


# Grid Search Optimiser
class GridSearchOptimiser():

    # initialise optimiser
    def __init__(self, fn, dim_bounds, verbose=False):
        self.fn = fn # instead of running NN and measuring the value
        self.dim_bounds = dim_bounds
        self.N = len(dim_bounds) # number of dimensions
        self.verbose = verbose

    # Create a matrix where each row 'i' represents one N-dimensional point.
    # Number of points is bounded to number of samples we want to measure.
    def __generateTestGridXCoordinates(self, samples):
        dim_samples = np.power(samples, 1 / float(self.N))
        if (dim_samples.is_integer()):
            dim_samples = int(dim_samples)
        else:
            dim_samples = int(dim_samples + 1)

        # create list of linear spaced arrays, each containing one N-dimensional point
        x_tmp = []
        for i in range(0,self.N):
            x_tmp.append(np.linspace(self.dim_bounds[i][0], self.dim_bounds[i][1], dim_samples))

        # create cartesian product of those arrays and convert it into matrix
        cartesian_tuples = [item for item in itertools.product(*x_tmp)]
        cartesian_list = [list(ele) for ele in cartesian_tuples]
        x_guess = np.array(cartesian_list)
        return x_guess

    # Divide the space given by dimension bounds with nodes and return the one
    # with the best result. Number of nodes is given by rounds.
    def optimise(self, rounds):
        y_all = []
        # find the node inside the grid with best result
        x_guess = self.__generateTestGridXCoordinates(rounds)
        for round in range(rounds):
            val = self.fn(x_guess[round])
            y_all.append(val)
            if (round == 0) or (best_value > val):
                x_best = x_guess[round]
                best_value = val
            if self.verbose:
                print("[round {}] settings: {}, value: {}".format(round + 1, x_guess[round], val))

        return (x_guess[:rounds]).transpose(), y_all, x_best, best_value
