#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# Gaussian Processes based OPtimiser (GPOP)                                    #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: This is the base of what will someday hopefully be hyper        #
# parameter omtimiser base on Gaussian processes...                            #
#------------------------------------------------------------------------------#

import numpy as np
import torch
import argparse
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import axes3d
import itertools
import random
from gpop.optimiser import optimiser as gpo
from gpop.optimiser import kernel
from gpop.optimiser import benchmarks
from gpop.nnbridge import bridge


# Draw GP prediction (1D parameter, 2D plot).
def drawPrediction2D(ax, x, fn, alpha, cmap, tr_x, tr_y, uncertainty=False):
    tmp_y = []
    tmp_pred = []
    tmp_x = x.T

    for i in range(0, len(tmp_x)):
        tmp = fn(tmp_x[i])
        next_y = (tmp[0]).item(0)
        tmp_y.append(next_y)
        next_pred = (tmp[1]).item(0)
        tmp_pred.append(next_pred)

    ax.set_facecolor('w')
    ax.plot(x[0], tmp_y, alpha=alpha)
    if uncertainty:
        ax.fill_between(x[0], tmp_y - 2 * np.sqrt(tmp_pred), tmp_y + 2 * np.sqrt(tmp_pred), color="#dddddd", alpha=0.5)

    # draw training poins
    for i in range(0, len(tr_x)):
        ax.scatter(tr_x[i], tr_y[i], c="k")

    ax.tick_params(axis='both', which='major', labelsize=18)
    ax.tick_params(axis='both', which='minor', labelsize=18)

# Draw GP prediction (2D parameter, 3D plot).
def drawPrediction3D(ax, x, fn, alpha, cmap, tr_x, tr_y, uncertainty=False):
    tmp_y = []
    tmp_sigma = []
    tmp_x = x.T
    # get Z coordinates for mean prediction and plot it
    for i in range(0, len(tmp_x)):
        next_y = (fn(tmp_x[i])[0]).item(0)
        next_sigma = (fn(tmp_x[i])[1]).item(0)
        tmp_y.append(next_y)
        tmp_sigma.append(next_sigma)

    # set axis limits
    tmp_y = np.array(tmp_y)
    tmp_sigma = np.array(tmp_sigma)
    max_range = np.array([x[0].max()-x[0].min(), x[1].max()-x[1].min(), tmp_y.max()-tmp_y.min()]).max() / 2.0
    mid_x = (x[0].max()+x[0].min()) * 0.5
    mid_y = (x[1].max()+x[1].min()) * 0.5
    mid_z = (tmp_y.max()+tmp_y.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(-max_range, mid_z + max_range)

    ax.plot_trisurf(x[0], x[1], tmp_y, alpha=alpha, cmap=cmap, linewidth=0, antialiased=True)
    if uncertainty:
        ax.plot_trisurf(x[0], x[1], tmp_y - np.sqrt(tmp_sigma) * 2, alpha=0.2, cmap=cm.get_cmap('binary'), linewidth=0, antialiased=True)
        ax.plot_trisurf(x[0], x[1], tmp_y + np.sqrt(tmp_sigma) * 2, alpha=0.2, cmap=cm.get_cmap('binary'), linewidth=0, antialiased=True)
    # draw training poins
    for i in range(0, len(tr_x)):
        ax.scatter(tr_x[i][0], tr_x[i][1], tr_y[i], s=30, c="k")
    # name axes
    ax.set_xlabel('h1', fontsize=22, labelpad=20)
    ax.set_ylabel('h2', fontsize=22, labelpad=20)
    ax.set_zlabel('f(h)', fontsize=22, labelpad=10)
    ax.set_facecolor('w')
    ax.tick_params(axis='both', which='major', labelsize=18)
    ax.tick_params(axis='both', which='minor', labelsize=18)

# Draw original function (1D parameter, 2D plot).
def drawOriginal2D(ax, fn, dim_bounds, nb_samples):
    x = np.linspace(dim_bounds[0][0], dim_bounds[0][1], nb_samples)
    y = []
    for i in range(len(x)):
        tmp = []
        tmp.append(x[i])
        y.append(fn(np.array(tmp)))
    ax.plot(x, y, color='darkorange', linestyle='--')

# Draw original function (2D parameter, 3D plot).
def drawOriginal3D(ax, fn, dim_bounds, nb_samples):
    if (len(dim_bounds) == 2):
        x = np.linspace(dim_bounds[0][0], dim_bounds[0][1], nb_samples)
        y = np.linspace(dim_bounds[1][0], dim_bounds[1][1], nb_samples)
        Z = []
        X, Y = np.meshgrid(x, y)
        # function fn() take args as array, so we need to compute it for each
        # point of the mesh separately
        for i in range (len(X)):
            tmp_z = []
            for j in range(len(X[i])):
                tmp = np.array([X[i][j], Y[i][j]])
                tmp_z.append(fn(tmp))
            Z.append(tmp_z)
        ax.plot_wireframe(X, Y, np.array(Z), color='0.3', alpha='0.3')
    else:
        print("Can't draw in {} dimensions".format(len(dim_bounds)))

# Draw cumulative minimum of the optimisation run.
def drawCumulativeMinimum(ax, rounds, y, label_name, colorMin="b", colorAct="r"):
    best_y = y[0] + 1
    best_all = []
    steps = list(range(len(y)))
    for j in steps:
        # plot value of each step
        ax.plot(j, y[j], c=colorAct, marker="o")
        if (y[j] <= best_y):
            best_y = y[j]
            best_all.append(y[j])
        else:
            best_all.append(best_y)
    # plot cumulative minimum
    ax.set_facecolor('w')
    ax.grid(color='0.75', linestyle='--', linewidth=1)
    ax.plot(steps, best_all, c=colorMin, label=label_name)
    ax.legend()

# Get dimensions bounds from command line.
def parseBenchmarkDimBounds(dims):
    dim_bounds = []
    for dim in dims:
        tmp_dim = []
        tmp = dim.split(":")
        tmp_dim.append(float(tmp[0]))
        tmp_dim.append(float(tmp[1]))
        dim_bounds.append(tmp_dim)

    return dim_bounds

# Get optimum from command line.
def parseOptimalValues(vals):
    list = vals.split()
    return np.array([float(i) for i in list])

# Generate coordinate grid for the 3D plot.
def generatePlotXCoordinates(dim_bounds, nb_samples):
    # create list of linear spaced arrays, each containing values of one dimension
    x_tmp = []
    for i in range(0,len(dim_bounds)):
        x_tmp.append(np.linspace(dim_bounds[i][0], dim_bounds[i][1], nb_samples))

    # create cartesian product of those arrays and convert it into matrix
    cartesian_tuples = [item for item in itertools.product(*x_tmp)]
    cartesian_list = [list(ele) for ele in cartesian_tuples]
    x_guess = np.array(cartesian_list)

    return x_guess.transpose()

# Print achieved cumulative minimum over the optimisation rounds.
def printCumulativeMinimum(y, best_x, verbose):
    if verbose:
        print("----------------------------------------")
        print("Cumulative minimum over rounds:")
    best_y = y[0] + 1
    steps = list(range(len(y)))
    for j in steps:
        if (y[j] <= best_y):
            best_y = y[j]
            if verbose:
                print("{}: {}".format(j + 1, y[j]))
            else:
                print(y[j])
        else:
            if verbose:
                print("{}: {}".format(j + 1, best_y))
            else:
                print(best_y)

    print("[RESULT] GPO best coordinates: {} value: {}".format(best_x, best_y))



#------------------------------------------------------------------------------#
#                                    Main                                      #
#------------------------------------------------------------------------------#
def main():
    # get optimisation settings from args
    parser = argparse.ArgumentParser(description="Quick hyper-optimization runner.")
    g_exclusive = parser.add_mutually_exclusive_group()
    mle_exclusive = parser.add_mutually_exclusive_group()

    # optimizer options
    parser.add_argument("-r", "--rounds", type=int, default=10)
    parser.add_argument("--grid-search", action="store_true")
    parser.add_argument("--random-search", action="store_true")
    parser.add_argument("--gaussian-process", nargs='*') # args: <kernel_name> <#samples> <R> <kernel_param>*
    parser.add_argument("--domain-search-strategy", choices=['grid', 'random'], default='random')
    parser.add_argument("--acquisition-function", choices=['MinimalMean', 'LowerConfidence', 'ExpectedImprovement'], default='ExpectedImprovement')
    mle_exclusive.add_argument("--autotune", action="store_true")
    mle_exclusive.add_argument("--autotune-all", action="store_true")

    # optimisation options
    g_exclusive.add_argument("-b", "--benchmark", nargs='+')
    g_exclusive.add_argument("-f", "--config-file")

    # additional (and debugging) options
    parser.add_argument("--jitter", type=float)
    parser.add_argument("-d", "--draw", action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("--round-by-round", action="store_true")
    parser.add_argument("-s", "--save-fig")
    parser.add_argument("--seed", type=int) # FOR EXPERIMENT PURPOSES
    fig_format = "png"
    args = parser.parse_args()

    rounds = args.rounds
    # FOR EXPERIMENTS:
    # It is helpful when each run selects same "random" point, so it is easier
    # to compare used methods and algorithms with each other. So for experiment
    # purposes, use same seed when we need to generate same "random" behaviour
    # (e.g. compare 2 kernels with 'random' domain space search strategy:
    # we want to see differences between kernels, not between several randomized
    # runs - so make random choose the same numbers for each two corresponding
    # runs).
    if args.seed is not None:
        random.seed(args.seed)

    # Handle optimisation problem params
    # optimising benchmark
    htypes = None # hyper-parameter value types (None -> default == float)
    if args.benchmark is not None:
        x_opt = parseOptimalValues(args.benchmark[1])
        f_opt = float(args.benchmark[2])
        tested_fn = benchmarks.selectBenchmark(args.benchmark[0], x_opt, f_opt, jitter=args.jitter)
        dim_bounds = parseBenchmarkDimBounds(args.benchmark[3:])
    # actual neural network will be optimised
    else:
        # configuration in file
        if args.config_file is not None:
            tested_fn, dim_bounds = bridge.parseFileConfig(args.config_file)
        # configuration from command line
        else:
            tested_fn, dim_bounds = bridge.parseStdInConfig()
        # create list of HP types
        htypes = []
        for hpar in tested_fn.hparams:
            htypes.append(hpar.type)

    # handle tool parameters (drawing, verbosity, etc.)
    draw_samples = int(100**(1.0/len(dim_bounds)))

    # print optimisation details
    if args.verbose:
        print("----------------------------------------")
        if args.benchmark is not None:
            print("Optimising benchmark: {}".format(tested_fn.__getStringRepresentation__()))
            print(" * optimal settings: {}".format(tested_fn.__getOptimum__()))
            print(" * minimal value:    {}".format(tested_fn.__getOptimalFunctionValue__()))
        else:
            print("Optimising hyper-parameters of neural network:")
            for hpar in tested_fn.hparams:
                print(" * {} [{}]: from {} to {}".format(hpar.name, hpar.type, hpar.min, hpar.max))
        print("----------------------------------------")

    # GAUSSIAN PROCESSES based OPTIMIZER
    if args.gaussian_process is not None:
        # handle GP optimizer params
        kernel_name = args.gaussian_process[0]
        gp_samples = int(args.gaussian_process[1])
        R = float(args.gaussian_process[2]) # FIXME: ERROR with R=0
        kernel_params = [float(i) for i in args.gaussian_process[3:]]
        kernel_params = torch.autograd.Variable(torch.from_numpy(np.array(kernel_params)), requires_grad=True) # create tensor

        # print GP optimizer details
        if args.verbose:
            print("----------------------------------------")
            print("GP settings:")
            print(" * samples/parameter: {}".format(gp_samples))
            print(" * dimensions [x_0[start, stop]...x_n[start, stop]]: {}".format(dim_bounds))
            print(" * used kernel: {}".format(kernel_name))
            print(" * R/kernel parameters: {}/{}".format(R, kernel_params))
            print("----------------------------------------")

        # setup Gaussian process and optimize
        best_mean = []
        k = kernel.selectKernel(kernel_name, kernel_params)
        opt = gpo.GPOptimiser(tested_fn, dim_bounds, k, R=R, nb_samples=gp_samples, dss=args.domain_search_strategy, acq_fun=args.acquisition_function, htypes=htypes, autotune=args.autotune, autotune_all=args.autotune_all, verbose=args.verbose)
        # round by round optimisation
        if args.round_by_round:
            for round in range(rounds):
                gp = opt.optimise(1)

                # print cumulative minimum
                printCumulativeMinimum(gp.y, opt.best_x, args.verbose)

                # draw prediction
                if args.draw:
                    dim_num = len(dim_bounds)
                    if dim_num == 1:
                        x = generatePlotXCoordinates(dim_bounds, draw_samples)
                        fig = plt.figure()
                        fig.suptitle('Gaussian Process optimiser')
                        ax = fig.add_subplot(111)
                        ax.grid(color='0.75', linestyle='--', linewidth=1)
                        drawPrediction2D(ax, x, gp.predict, 0.8, cm.coolwarm, gp.x, gp.y, uncertainty=True)
                        if args.benchmark is not None:
                            drawOriginal2D(ax, tested_fn, dim_bounds, draw_samples)
                        if args.save_fig is not None:
                            filename = "{}/prediction_{}.{}".format(args.save_fig, round, fig_format)
                            fig.savefig(filename, bbox_inches='tight')
                        plt.show(block=False)
                    elif dim_num == 2:
                        x = generatePlotXCoordinates(dim_bounds, draw_samples)
                        fig = plt.figure()
                        fig.suptitle('Gaussian Process optimiser')
                        ax = fig.add_subplot(111, projection='3d')
                        drawPrediction3D(ax, x, gp.predict, 0.8, cm.coolwarm, gp.x, gp.y)
                        if args.benchmark is not None:
                            drawOriginal3D(ax, tested_fn, dim_bounds, draw_samples)
                        if args.save_fig is not None:
                            filename = "{}/prediction_{}.{}".format(args.save_fig, round, fig_format)
                            fig.savefig(filename, bbox_inches='tight')
                        plt.show(block=False)
                    else:
                        print("[WARNING] Can't draw in {} dimensions....")
        # optimize all rounds at once
        else:
            gp = opt.optimise(rounds)

            # print cumulative minimum
            printCumulativeMinimum(gp.y, opt.best_x, args.verbose)

            # draw prediction
            if args.draw:
                dim_num = len(dim_bounds)
                if dim_num == 1:
                    x = generatePlotXCoordinates(dim_bounds, draw_samples)
                    fig = plt.figure()
                    fig.suptitle('Gaussian Process optimiser')
                    ax = fig.add_subplot(111)
                    ax.grid(color='0.75', linestyle='--', linewidth=1)
                    drawPrediction2D(ax, x, gp.predict, 0.8, cm.coolwarm, gp.x, gp.y, uncertainty=True)
                    if args.benchmark is not None:
                        drawOriginal2D(ax, tested_fn, dim_bounds, draw_samples)
                    if args.save_fig is not None:
                        filename = "{}/prediction.{}".format(args.save_fig, fig_format)
                        fig.savefig(filename, bbox_inches='tight')
                    plt.show(block=False)
                elif dim_num == 2:
                    x = generatePlotXCoordinates(dim_bounds, draw_samples)
                    fig = plt.figure()
                    fig.suptitle('Gaussian Process optimiser')
                    ax = fig.add_subplot(111, projection='3d')
                    drawPrediction3D(ax, x, gp.predict, 0.8, cm.coolwarm, gp.x, gp.y)
                    if args.benchmark is not None:
                        drawOriginal3D(ax, tested_fn, dim_bounds, draw_samples)
                    if args.save_fig is not None:
                        filename = "{}/prediction.{}".format(args.save_fig, fig_format)
                        fig.savefig(filename, bbox_inches='tight')
                    plt.show(block=False)
                else:
                    print("[WARNING] Can't draw in {} dimensions....")

    # GRID SEARCH based OPTIMIZER
    if args.grid_search:
        if args.verbose:
            print("----------------------------------------")
        opt =  gpo.GridSearchOptimiser(tested_fn, dim_bounds, verbose=args.verbose)
        grid_x, grid_y, grid_best_x, grid_best_y = opt.optimise(rounds)

        # print cumulative minimum
        if args.verbose:
            print("----------------------------------------")
            print("Cumulative minimum over rounds:")
        best_y = grid_y[0] + 1
        steps = list(range(len(grid_y)))
        for j in steps:
            if (grid_y[j] <= best_y):
                best_y = grid_y[j]
                if args.verbose:
                    print("{}: {}".format(j + 1, grid_y[j]))
                else:
                    print(grid_y[j])
            else:
                if args.verbose:
                    print("{}: {}".format(j + 1, best_y))
                else:
                    print(best_y)

        print("[RESULT] GridSearch best coordinates: {} value: {}".format(grid_best_x, grid_best_y))

    # RANDOM SEARCH based OPTIMIZER
    if args.random_search:
        if args.verbose:
            print("----------------------------------------")
        opt =  gpo.RandomSearchOptimiser(tested_fn, dim_bounds, verbose=args.verbose)
        rand_x, rand_y, rand_best_x, rand_best_y = opt.optimise(rounds)

        # Experiments: for each round, print best_y
        if args.verbose:
            print("----------------------------------------")
            print("Cumulative minimum over rounds:")
        best_y = rand_y[0] + 1
        steps = list(range(len(rand_y)))
        for j in steps:
            if (rand_y[j] <= best_y):
                best_y = rand_y[j]
                if args.verbose:
                    print("{}: {}".format(j + 1, rand_y[j]))
                else:
                    print(rand_y[j])
            else:
                if args.verbose:
                    print("{}: {}".format(j + 1, best_y))
                else:
                    print(best_y)

        print("[RESULT] RandomSearch best coordinates: {} value: {}".format(rand_best_x, rand_best_y))

    # DRAWING
    if args.draw:
        # draw cumulative minimum
        fig = plt.figure()
        fig.suptitle('Cumulative minimum over time')
        plt.xlabel('step')
        plt.ylabel('function value')
        ax = fig.add_subplot()
        if args.gaussian_process is not None:
            drawCumulativeMinimum(ax, rounds, gp.y, "GPO", colorMin="b", colorAct="b")
        if args.grid_search:
            drawCumulativeMinimum(ax, rounds, grid_y, "GridSearch", colorMin="r", colorAct="r")
        if args.random_search:
            drawCumulativeMinimum(ax, rounds, rand_y, "RandomSearch", colorMin="g", colorAct="g")
        if args.save_fig is not None:
            filename = "{}/cumulative_minimum.{}".format(args.save_fig, fig_format)
            fig.savefig(filename, bbox_inches='tight')
        plt.show()


if __name__== "__main__":
    main()
