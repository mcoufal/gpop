#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# GPOP - Bridge between optimizer and NNs                                      #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: An interface between Gaussian Process based optimizer and common#
# neural network. Provides an easy way to load neural network configuration and#
# transform it into a callable object, that can be used by optimiser.          #
#------------------------------------------------------------------------------#

from subprocess import Popen, PIPE
import sys
import re

# Get NN settings from its configuration.
def parseConfig(config):
    hparams = []
    dim_bounds = []
    for i, line in enumerate(config):
        line = line[:-1] # get rid of newline characters
        # command to run NN
        if (i == 0):
            cmd = line.split()
        # regex for acceptable value
        elif (i == 1):
            re_res = line
        # regex for unacceptable values like NaN aso.
        elif (i == 2):
            re_fail = line
        # hyper-parameter settings
        else:
            hpar = HParam(*line.split())
            hparams.append(hpar)
            dim_bounds.append([float(hpar.min), float(hpar.max)])
    return cmd, re_res, re_fail, hparams, dim_bounds

# Get NN settings stored in the configuration file given by @file_path.
def parseFileConfig(file_path):
    with open(file_path) as file:
        cmd, re_res, re_fail, hparams, dim_bounds = parseConfig(file)
    return NeuralNet(cmd, re_res, re_fail, hparams), dim_bounds

# Get NN settings stored in the configuration file given by standard input.
def parseStdInConfig():
    lines = sys.stdin.readlines()
    cmd, re_res, re_fail, hparams, dim_bounds = parseConfig(lines)
    return NeuralNet(cmd, re_res, re_fail, hparams), dim_bounds


# Black-box function-like representation of the NN.
class NeuralNet():

    # Creates new NeuralNet using given settings.
    def __init__(self, cmd, re_res, re_fail, hparams, verbose=False):
        self.cmd = cmd
        self.re_res = re_res
        self.re_fail = re_fail
        self.hparams = hparams
        self.verbose = verbose

    # Returns list of commands to be run along with their arguments.
    def __getCmdWithArgs(self):
        args = []
        for i in range(len(self.hparams)):
            args.append(self.hparams[i].name)
            args.append(self.hparams[i].val)
        return self.cmd + args

    # Update hyper-parameters values, based on given @x.
    def __updateConfig(self, x):
        for i in range(len(x)):
            if self.hparams[i].type == "float":
                self.hparams[i].val = str(x[i])
            elif self.hparams[i].type == "int":
                self.hparams[i].val = str(int(round(x[i])))
            elif self.hparams[i].type == "cat":
                raise NotImplementedError("Categorical hyper-parameter values not implemented yet!")
            else:
                raise ValueError("Hyper-parameter type '{}' not valid!".format(self.hparams[i].type))

    # Callable interface for optimizers. Takes input array @x and returns loss.
    def __call__(self, x):
        encoding = 'utf-8' # FIXME: this is not very nifty solution...
        self.__updateConfig(x)
        cmd = self. __getCmdWithArgs()
        if self.verbose:
            print("...running '{}'".format(cmd))
        p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, err = p.communicate()
        rc = p.returncode

        # match result value
        output = output.decode(encoding)
        result = re.findall(self.re_res, output)
        if result:
            return float(result[-1])
        else:
            fail = re.findall(self.re_fail, output)
            if fail:
                return float("inf")
            else:
                print("[ERROR]: Failed to find loss given by regex '{}' in neural networks output '{}'.".format(self.re_res, output))
                exit(-1)


# Hyper-parameter represantation.
class HParam():

    # initialize
    def __init__(self, name, type, min, max):
        self.name = name
        self.type = type
        self.min = min
        self.max = max
        self.val = None
