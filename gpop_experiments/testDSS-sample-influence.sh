#!/usr/bin/env bash
#------------------------------------------------------------------------------#
# Gaussian Processes based OPtimiser (GPOP) EXPERIMENTS with DSS               #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Run DSS experiments for different number of samples per         #
# hyper-parameter to observe it's influence on accuracy (for each DSS)         #
#------------------------------------------------------------------------------#

export SPECIALIZATION="nsamples"

# experiments in 1D
SAMPLE_NUMBERS="2 4 8 16 32 64 128 256 512 1024"
for SAMPLE in $SAMPLE_NUMBERS; do
    export SAMPLES=${SAMPLE}
    ./testDSS-1D.sh
done

# experiments in 2D
# Number of samples is per dimension, which means total number of samples used
# is for two dimensions actually:
#   4 16 64 256 1024 4096 16384
SAMPLE_NUMBERS="2 4 8 16 32 64 128"
for SAMPLE in $SAMPLE_NUMBERS; do
    export SAMPLES=${SAMPLE}
    ./testDSS-2D.sh
done

# experiments in 3D
# Number of samples is per dimension, which means total number of samples used
# is for two dimensions actually:
#   8 64 512 1728 4096 13824 32768
SAMPLE_NUMBERS="2 4 8 12 16 24 32"
for SAMPLE in $SAMPLE_NUMBERS; do
    export SAMPLES=${SAMPLE}
    ./testDSS-3D.sh
done
