#!/usr/bin/env bash
#------------------------------------------------------------------------------#
# Gaussian Processes based OPtimiser (GPOP) EXPERIMENTS data backup            #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Compress data folder and replace current data backup.           #
#------------------------------------------------------------------------------#


DATA_FOLDER="./data/"
ARCHIVE="./data_backup.tar.gz"

echo "[INFO] removing old backup: '$ARCHIVE'..."
rm -rf $ARCHIVE
echo "[INFO] compression of '$DATA_FOLDER' started..."
tar -czvf $ARCHIVE $DATA_FOLDER
if [ $? == 0 ]; then
    echo "[SUCCESS]"
else
    echo "[ERROR] Something went terribly wrong..."
fi
