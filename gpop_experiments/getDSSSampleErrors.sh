#!/usr/bin/env bash
#------------------------------------------------------------------------------#
# Gaussian Processes based OPtimiser (GPOP) get DSS sample influence data      #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Get DSS experiments data for different number of samples per    #
# hyper-parameter to observe it's influence on accuracy (for each DSS)         #
#------------------------------------------------------------------------------#

DIR="/home/xcoufa08/Documents/FIT/MIT/3-MIT/gpop/gpop_experiments/data/DSS/nsamples_kern-KernelRBF_af-LowerConfidence_bench-FnEllipsoidal"
EXPERIMENTS=`ls $DIR`


for exp in $EXPERIMENTS; do
    echo ${exp}
    cat ${DIR}/${exp}/avg.txt | nl
    echo "-----"
done
