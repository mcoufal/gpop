#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# Plot sample count influence.                                                       #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Plot influence of number of samples with specific DSS on        #
# accuracy.                                                                    #
#------------------------------------------------------------------------------#

import os
import numpy as np
import argparse
from matplotlib import pyplot as plt
import seaborn as sns
sns.set(color_codes=True)
import sys


def drawSampleInfluence(ax, steps, y, label_name):
    ax.plot(steps, y, label=label_name, marker='o')
    ax.set_facecolor('w')
    ax.set_xlabel('number of samples', fontsize=30, labelpad=5)
    ax.set_ylabel('error', fontsize=30, labelpad=5)
    ax.tick_params(axis='both', which='major', labelsize=30)
    ax.tick_params(axis='both', which='minor', labelsize=30)
    ax.legend()


plt.rc('legend',fontsize=30)

# GRID 1D data

x = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]

y1 = np.array([4.4738035888, # 2
     4.1454989387,  # 4
     3.4610429242,  # 8
     5.0908073647,  # 16
     4.2959724301,  # 32
     3.3762389629,  # 64
     4.1762500475,  # 128
     4.4492160793,  # 256
     4.5609687906,  # 512
     3.9681274484])  # 1024

y2 = np.array([1.1928659775, # 2
    1.4651900477,  # 4
    1.1757663727,  # 8
    1.2376418603,  # 16
    1.4267359066,  # 32
    1.1522236386,  # 64
    1.2388131450,  # 128
    1.4102350104,  # 256
    1.2397442756,  # 512
    1.3270628666])  # 1024

y3 = np.array([1.0265541936, # 2
    .4145847570,  # 4
    .5017909785,  # 8
    .3608684060,  # 16
    .4175796689,  # 32
    .4251008061,  # 64
    .4348031448,  # 128
    .4457972836,  # 256
    .3578953634,  # 512
    .4184010508])  # 1024

y4 = np.array([.9666795397, # 2
    .2100968946,  # 4
    .1451663048,  # 8
    .1472316563,  # 16
    .1010785512,  # 32
    .1212905905,  # 64
    .0772947638,  # 128
    .1038944613,  # 256
    .1100821264,  # 512
    .1321656687])  # 1024

y5 = np.array([.9320652588,
    .1672803331,
    .0386570677,
    .0252861071,
    .0309446723,
    .0273627906,
    .0290425456,
    .0188753643,
    .0298306473,
    .0364220969])

y6 = np.array([.9193862240,
    .1552707980,
    .0327118249,
    .0189945094,
    .0148720015,
    .0146490900,
    .0109545319,
    .0129499216,
    .0138225237,
    .0144293484])

y7 = np.array([.8968105052,
    .1495537127,
    .0305770398,
    .0159918051,
    .0121308660,
    .0122281483,
    .0099790788,
    .0113066687,
    .0129153195,
    .0131771396])

y8 = np.array([.8865611864,
    .1472926194,
    .0296890988,
    .0155452523,
    .0111528183,
    .0119046515,
    .0097542821,
    .0111438113,
    .0125245867,
    .0131321672])

y9 = np.array([.8732715630,
    .1426923941,
    .0289743391,
    .0151377717,
    .0107767788,
    .0116794425,
    .0097163515,
    .0111022886,
    .0122118442,
    .0131060604])

y10 = np.array([.8708578637,
    .1406598607,
    .0282301924,
    .0140095963,
    .0106757191,
    .0116028291,
    .0096933531,
    .0110958299,
    .0121644474,
    .0126227462])

# Plot
fig = plt.figure()
#fig.suptitle('grid DSS 1D')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.set_ylim([0,110])
ax.grid(color='0.75', linestyle='--', linewidth=1)
#drawSampleInfluence(ax, x, y1, "Step 1")
#drawSampleInfluence(ax, x, y2, "Step 2")
#drawSampleInfluence(ax, x, y3, "Step 3")
drawSampleInfluence(ax, x, y4, "Step 4")
#drawSampleInfluence(ax, x, y5, "Step 5")
drawSampleInfluence(ax, x, y6, "Step 6")
#drawSampleInfluence(ax, x, y7, "Step 7")
drawSampleInfluence(ax, x, y8, "Step 8")
#drawSampleInfluence(ax, x, y9, "Step 9")
drawSampleInfluence(ax, x, y10, "Step 10")
plt.show()

y_avg1 = (y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9 + y10) / 10


# RANDOM 1D data

x = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]

y1 = np.array([4.4738035888,
    4.1454989387,
    3.4610429242,
    5.0908073647,
    4.2959724301,
    3.3762389629,
    4.1762500475,
    4.4492160793,
    4.5609687906,
    3.9681274484])

y2 = np.array([1.2748372455,
    1.3746132923,
    .9848006674,
    .9966925497,
    1.3519185696,
    1.1479909222,
    1.2161238495,
    1.3931570367,
    1.2307618043,
    1.3231085695])

y3 = np.array([.6183566662,
    .4548702651,
    .3335418401,
    .2968898543,
    .3615169210,
    .3927668126,
    .4170998031,
    .4356738686,
    .3555477680,
    .4163644920])

y4 = np.array([.3460279645,
    .1419244387,
    .1344343592,
    .1076107518,
    .0856667006,
    .1096010259,
    .0817960334,
    .0995853941,
    .1087984489,
    .1311743823])

y5 = np.array([.2159463438,
    .1060356987,
    .0623873658,
    .0423263669,
    .0259496389,
    .0198508183,
    .0329606463,
    .0165077981,
    .0306118930,
    .0355985014])

y6 = np.array([.1455883758,
    .0617963556,
    .0292195475,
    .0175675782,
    .0114888182,
    .0068402155,
    .0073301443,
    .0106809198,
    .0122936958,
    .0135989660])

y7 = np.array([.1349010522,
    .0397534495,
    .0190304805,
    .0090068553,
    .0058944037,
    .0055251553,
    .0054666043,
    .0094989014,
    .0113792319,
    .0128531248])

y8 = np.array([.0516647267,
    .0262760276,
    .0100413063,
    .0052933650,
    .0040999424,
    .0043205441,
    .0051884108,
    .0084012759,
    .0113016047,
    .0122897191])

y9 = np.array([.0444102217,
    .0186144541,
    .0071541402,
    .0031729419,
    .0029709086,
    .0036463943,
    .0047596332,
    .0080857974,
    .0107098647,
    .0117160253])

y10 = np.array([.0373176732,
    .0136101974,
    .0054054605,
    .0022633641,
    .0020278293,
    .0029976327,
    .0046382517,
    .0078712573,
    .0103561278,
    .0115514429])

# Plot
fig = plt.figure()
#fig.suptitle('random DSS 1D')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.set_ylim([0,110])
ax.grid(color='0.75', linestyle='--', linewidth=1)
#drawSampleInfluence(ax, x, y1, "Step 1")
#drawSampleInfluence(ax, x, y2, "Step 2")
#drawSampleInfluence(ax, x, y3, "Step 3")
drawSampleInfluence(ax, x, y4, "Step 4")
#drawSampleInfluence(ax, x, y5, "Step 5")
drawSampleInfluence(ax, x, y6, "Step 6")
#drawSampleInfluence(ax, x, y7, "Step 7")
drawSampleInfluence(ax, x, y8, "Step 8")
#drawSampleInfluence(ax, x, y9, "Step 9")
drawSampleInfluence(ax, x, y10, "Step 10")
plt.show()

y_avg2 = (y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9 + y10) / 10
# Plot
fig = plt.figure()
#fig.suptitle('Average DSS 1D')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.set_ylim([0,110])
ax.grid(color='0.75', linestyle='--', linewidth=1)
drawSampleInfluence(ax, x, y_avg1, "Average - grid")
drawSampleInfluence(ax, x, y_avg2, "Average - random")
plt.show()

# GRID 2D data

x = [4, 16, 64, 256, 1024, 4096, 16384]

y1 = np.array([7.7952522119,
    7.5139931867,
    8.7905674491,
    7.1656763526,
    8.3349638873,
    8.5289311221,
    7.6308396426])

y2 = np.array([4.6983549216,
    4.3026137614,
    4.8637279851,
    4.8485914473,
    5.0023674218,
    5.3451718065,
    5.0215903525])

y3 = np.array([3.7657540493,
    3.3556515136,
    3.5381143522,
    3.5515932196,
    3.7783010367,
    4.3125668978,
    3.8455221265])

y4 = np.array([2.7129434788,
    2.5787674583,
    2.9477112500,
    2.6535055570,
    2.8094095098,
    2.9735246403,
    2.7806133392])

y5 = np.array([2.6225987370,
    1.7086257406,
    1.7285065200,
    1.8421619251,
    1.8147051753,
    1.5410042146,
    1.7655544351])

y6 = np.array([2.4569613440,
    .9731699565,
    1.0494100649,
    1.2328801264,
    1.2855395228,
    1.0604054743,
    1.2056755734])

y7 = np.array([2.433803473,
    .7133608204,
    .7799181189,
    .9015108321,
    .8835881980,
    .8833249439,
    .9415520738])

y8 = np.array([2.358446127,
    .5928081338,
    .7386691172,
    .8175353099,
    .8069762067,
    .8177931654,
    .7711639011])

y9 = np.array([2.337162685,
    .5108783205,
    .6699214179,
    .7904353570,
    .7456902297,
    .7069269277,
    .6942708233])

y10 = np.array([2.320507797,
    .4406507984,
    .3991159599,
    .3529176027,
    .4744435418,
    .4388961911,
    .3519882933])

y11 = np.array([2.279457065,
    .3889783357,
    .2195831640,
    .1556961505,
    .2198163925,
    .2035660570,
    .1991419045])

y12 = np.array([2.260242759,
    .3590084447,
    .1271450812,
    .0957944005,
    .1174310966,
    .1287158155,
    .0971891486])

y13 = np.array([2.251420914,
    .3437837146,
    .0990985801,
    .0537619404,
    .0568079259,
    .0519841089,
    .0487830111])

y14 = np.array([2.242187955,
    .3330008496,
    .0731689550,
    .0398480083,
    .0338580037,
    .0314513095,
    .0321240186])

y15 = np.array([2.237440422,
    .3297963694,
    .0685793053,
    .0329056755,
    .0281615818,
    .0243276299,
    .0253036208])

# Plot
fig = plt.figure()
#fig.suptitle('grid DSS 2D')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.set_ylim([0,110])
ax.grid(color='0.75', linestyle='--', linewidth=1)
#drawSampleInfluence(ax, x, y1, "Step 1")
#drawSampleInfluence(ax, x, y2, "Step 2")
#drawSampleInfluence(ax, x, y3, "Step 3")
#drawSampleInfluence(ax, x, y4, "Step 4")
#drawSampleInfluence(ax, x, y5, "Step 5")
drawSampleInfluence(ax, x, y6, "Step 6")
#drawSampleInfluence(ax, x, y7, "Step 7")
#drawSampleInfluence(ax, x, y8, "Step 8")
drawSampleInfluence(ax, x, y9, "Step 9")
#drawSampleInfluence(ax, x, y10, "Step 10")
#drawSampleInfluence(ax, x, y11, "Step 11")
drawSampleInfluence(ax, x, y12, "Step 12")
#drawSampleInfluence(ax, x, y13, "Step 13")
#drawSampleInfluence(ax, x, y14, "Step 14")
drawSampleInfluence(ax, x, y15, "Step 15")
plt.show()

y_avg1 = (y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9 + y10 + y11 + y12 + y13 + y14 + y15) / 15

# RANDOM 2D data

x = [4, 16, 64, 256, 1024, 4096, 16384]

y1 = np.array([7.7952522119,
    7.5139931867,
    8.7905674491,
    7.1656763526,
    8.3349638873,
    8.5289311221,
    7.6308396426])


y2 = np.array([3.8328485374,
    3.9431688477,
    4.2209477456,
    4.5679481013,
    4.8256102186,
    5.2390774338,
    4.9971465371])

y3 = np.array([2.0961307159,
    2.4563419283,
    2.9398422491,
    3.4407942438,
    3.7747127313,
    3.9725397832,
    3.8086750671])

y4 = np.array([1.4882330065,
    1.8549161268,
    2.1344153928,
    2.2804547708,
    2.5382296219,
    2.8584916321,
    2.7153725315])

y5 = np.array([1.1402272114,
    1.0163104420,
    1.2307792259,
    1.6358713684,
    1.5889237839,
    1.5466095112,
    1.6852719045])

y6 = np.array([.8237746495,
    .8098509414,
    .9417306622,
    .9581730459,
    1.1603603776,
    1.0092922307,
    1.1835583612])

y7 = np.array([.6998109355,
    .5741139045,
    .6106912963,
    .7029117605,
    .7998289863,
    .8360585323,
    .8823964895])

y8 = np.array([.6375047557,
    .3958374791,
    .4764952281,
    .6345576939,
    .7360574706,
    .7517132453,
    .7866374204])

y9 = np.array([.5690364394,
    .2920160150,
    .3706031892,
    .5177462456,
    .6642442455,
    .6625193156,
    .6927887569])

y10 = np.array([.4898842054,
    .2299537493,
    .2525817471,
    .2648542710,
    .4024936836,
    .4099946944,
    .3365592979])

y11 = np.array([.4191477089,
    .1734689431,
    .1525844827,
    .1480356187,
    .1951644163,
    .1862787705,
    .1897713085])

y12 = np.array([.3788290350,
    .1440780158,
    .1124877234,
    .0869024018,
    .1209536738,
    .1243501027,
    .1152004400])

y13 = np.array([.3379475923,
    .1205785309,
    .0635511004,
    .0536258295,
    .0675213610,
    .0495156535,
    .0484032065])

y14 = np.array([.2890072690,
    .0922459233,
    .0427169641,
    .0344735793,
    .0409436974,
    .0295777843,
    .0353021791])

y15 = np.array([.2694510277,
    .0815504807,
    .0334698966,
    .0190076122,
    .0241451661,
    .0216989717,
    .0286493792])

# Plot
fig = plt.figure()
#fig.suptitle('random DSS 2D')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.set_ylim([0,110])
ax.grid(color='0.75', linestyle='--', linewidth=1)
#drawSampleInfluence(ax, x, y1, "Step 1")
#drawSampleInfluence(ax, x, y2, "Step 2")
#drawSampleInfluence(ax, x, y3, "Step 3")
#drawSampleInfluence(ax, x, y4, "Step 4")
#drawSampleInfluence(ax, x, y5, "Step 5")
drawSampleInfluence(ax, x, y6, "Step 6")
#drawSampleInfluence(ax, x, y7, "Step 7")
#drawSampleInfluence(ax, x, y8, "Step 8")
drawSampleInfluence(ax, x, y9, "Step 9")
#drawSampleInfluence(ax, x, y10, "Step 10")
#drawSampleInfluence(ax, x, y11, "Step 11")
drawSampleInfluence(ax, x, y12, "Step 12")
#drawSampleInfluence(ax, x, y13, "Step 13")
#drawSampleInfluence(ax, x, y14, "Step 14")
drawSampleInfluence(ax, x, y15, "Step 15")
plt.show()

y_avg2 = (y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9 + y10 + y11 + y12 + y13 + y14 + y15) / 15
# Plot
fig = plt.figure()
#fig.suptitle('Average DSS 2D')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.set_ylim([0,110])
ax.grid(color='0.75', linestyle='--', linewidth=1)
drawSampleInfluence(ax, x, y_avg1, "Average - grid")
drawSampleInfluence(ax, x, y_avg2, "Average - random")
plt.show()

# GRID 3D data

x = [8, 64, 512, 1728, 4096, 13824, 32768]

y1 = np.array([12.2042243151,
    10.8837284719,
    12.6745512818,
    12.4666452107,
    12.4350066373,
    13.5162465560,
    12.2312523807])

y2 = np.array([9.5329341384,
    8.9740579704,
    10.0984919357,
    8.7263729334,
    9.4568733531,
    8.8293801333,
    9.2214262739])

y3 = np.array([8.3315624913,
    7.3999627849,
    7.6124637667,
    7.0914278565,
    7.5087427968,
    6.8896835851,
    7.4859292395])

y4 = np.array([7.4831500161,
    6.3601190481,
    6.8277861236,
    5.9071288927,
    6.6572140295,
    5.7928068249,
    6.4528741523])

y5 = np.array([6.9283798074,
    5.4630239516,
    5.5733074225,
    5.5375393247,
    5.8292407437,
    5.2742537064,
    5.3872252111])

y6 = np.array([6.0570056941,
    5.0132511307,
    5.3955056862,
    5.2138432891,
    5.1923388328,
    4.8950747742,
    4.9894352942])

y7 = np.array([5.6697152687,
    4.5533935715,
    4.7236868231,
    4.5877060007,
    4.5858439034,
    4.3900864646,
    4.6085181660])

y8 = np.array([5.4040291989,
    4.4982322591,
    4.2006624020,
    3.9393366711,
    4.0480315422,
    3.9660844501,
    4.1788866931])

y9 = np.array([5.2870030454,
    3.5102816245,
    3.3351696181,
    3.2013110448,
    3.0920889945,
    3.1824049654,
    3.1729268057])

y10 = np.array([4.9925761686,
    2.5795291356,
    2.8187449741,
    2.6626823816,
    2.7271174512,
    2.7727355899,
    2.6415927975])

y11 = np.array([4.8222620965,
    1.9808301332,
    2.2423181263,
    2.3047152069,
    2.4618799063,
    2.3175865987,
    2.3993719675])

y12 = np.array([4.7124518207,
    1.7751579744,
    2.0562353057,
    2.1486411996,
    2.2379552005,
    2.1392446293,
    2.1348156705])

y13 = np.array([4.6567748108,
    1.5382972616,
    1.8949318878,
    1.9613007538,
    1.9114884010,
    1.8647871977,
    2.0497761399])

y14 = np.array([4.6076012069,
    1.3566742746,
    1.7280864212,
    1.7743122809,
    1.7121584459,
    1.7578983878,
    1.8472184041])

y15 = np.array([4.5496770127,
    1.2938448690,
    1.5091921897,
    1.6565301722,
    1.5723841473,
    1.5853012423,
    1.5805485890])

y16 = np.array([4.5107959695,
    1.1869539099,
    1.4022778893,
    1.5951727376,
    1.4139426282,
    1.4108301539,
    1.4465125534])

y17 = np.array([4.4588859187,
    1.1372884117,
    1.3800687242,
    1.4733982339,
    1.3501265328,
    1.3944464162,
    1.3827196390])

y18 = np.array([4.4350510483,
    1.0603842931,
    1.3334883670,
    1.3918384962,
    1.2828485907,
    1.3822254685,
    1.3516641491])

y19 = np.array([4.4273068061,
    1.0100700470,
    1.2219402171,
    1.3050887413,
    1.2209037940,
    1.2869178564,
    1.2672111987])

y20 = np.array([4.4006813242,
    .9636123191,
    1.1582179028,
    1.2334683143,
    1.1338845916,
    1.1969195117,
    1.1616971855])

# Plot
fig = plt.figure()
#fig.suptitle('grid DSS 3D')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.set_ylim([0,110])
ax.grid(color='0.75', linestyle='--', linewidth=1)
#drawSampleInfluence(ax, x, y1, "Step 1")
#drawSampleInfluence(ax, x, y2, "Step 2")
#drawSampleInfluence(ax, x, y3, "Step 3")
#drawSampleInfluence(ax, x, y4, "Step 4")
drawSampleInfluence(ax, x, y5, "Step 5")
#drawSampleInfluence(ax, x, y6, "Step 6")
#drawSampleInfluence(ax, x, y7, "Step 7")
drawSampleInfluence(ax, x, y8, "Step 8")
#drawSampleInfluence(ax, x, y9, "Step 9")
#drawSampleInfluence(ax, x, y10, "Step 10")
drawSampleInfluence(ax, x, y11, "Step 11")
#drawSampleInfluence(ax, x, y12, "Step 12")
#drawSampleInfluence(ax, x, y13, "Step 13")
drawSampleInfluence(ax, x, y14, "Step 14")
#drawSampleInfluence(ax, x, y15, "Step 15")
#drawSampleInfluence(ax, x, y16, "Step 16")
drawSampleInfluence(ax, x, y17, "Step 17")
#drawSampleInfluence(ax, x, y18, "Step 18")
#drawSampleInfluence(ax, x, y19, "Step 19")
drawSampleInfluence(ax, x, y20, "Step 20")
plt.show()

y_avg1 = (y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9 + y10 + y11 + y12 + y13 + y14 + y15 + y16 + y17 + y18 + y19 + y20) / 20


# RANDOM 3D data

x = [8, 64, 512, 1728, 4096, 13824, 32768]

y1 = np.array([12.2042243151,
    10.8837284719,
    12.6745512818,
    12.4666452107,
    12.4350066373,
    13.5162465560,
    12.2312523807])

y2 = np.array([7.3345227146,
    7.8376721998,
    9.1013608969,
    8.1678495485,
    9.1635797557,
    8.7187040959,
    9.0376213374])

y3 = np.array([6.3056312196,
    6.1533673127,
    6.9505818386,
    7.1384241251,
    7.1157338645,
    6.5164314769,
    7.5471777614])

y4 = np.array([4.7233059569,
    5.0748856957,
    5.8422633196,
    5.8687599170,
    5.9869002914,
    5.5858604249,
    6.2345322972])

y5 = np.array([3.9677569265,
    4.3860982227,
    4.9755391397,
    4.9478853006,
    4.9039984142,
    4.7335892613,
    4.9412718723])

y6 = np.array([3.2104840114,
    3.7390043429,
    4.5085271607,
    4.3299850549,
    4.5010377544,
    4.4921216736,
    4.7417685536])

y7 = np.array([2.6743527489,
    3.1478007777,
    3.8802276021,
    3.8758461654,
    3.9066622309,
    4.0586355074,
    4.5401382451])

y8 = np.array([2.1426492146,
    2.5340725953,
    3.3188982476,
    3.1547476016,
    3.5121413658,
    3.5754339727,
    3.7905955509])

y9 = np.array([1.8404796932,
    2.1498981337,
    2.7673134817,
    2.7308416460,
    2.7474138236,
    2.8988769814,
    2.8596451935])

y10 = np.array([1.5763287676,
    1.9066778632,
    2.1608897207,
    2.1920031308,
    2.3481375100,
    2.4080270483,
    2.4484579688])

y11 = np.array([1.4376775661,
    1.6468110195,
    1.8825048682,
    1.8763407048,
    2.2178999718,
    2.1310178376,
    2.1762686385])

y12 = np.array([1.2298053353,
    1.4206910123,
    1.6649691817,
    1.7876947201,
    1.8612135535,
    1.8981426123,
    1.9306261809])

y13 = np.array([1.1225367933,
    1.3239424105,
    1.5445028949,
    1.5706102725,
    1.6145093697,
    1.7208608165,
    1.7788063180])

y14 = np.array([1.0247774596,
    1.1946655633,
    1.3935361192,
    1.4412455006,
    1.4422534928,
    1.5942696698,
    1.6893864141])

y15 = np.array([.9543127168,
    1.0765349324,
    1.2631071551,
    1.3816632961,
    1.3410386445,
    1.4320451653,
    1.3901285547])

y16 = np.array([.8640268102,
    .9240283146,
    1.1485781426,
    1.2839959184,
    1.2471653004,
    1.3466147098,
    1.2492104470])

y17 = np.array([.8418569565,
    .8168505021,
    1.0176396770,
    1.1662625083,
    1.1841631255,
    1.2580324908,
    1.1951295593])

y18 = np.array([.7983700766,
    .7018967873,
    .8781867248,
    1.0433743567,
    1.1096533364,
    1.1846607711,
    1.1671782681])

y19 = np.array([.7241945530,
    .6395794230,
    .8004454448,
    1.0019639312,
    .9809528147,
    1.1572034883,
    1.0821691905])

y20 = np.array([.6644153048,
    .5544873981,
    .7318482342,
    .8968196746,
    .8734023680,
    1.0382710741,
    .9963018773])

# Plot
fig = plt.figure()
#fig.suptitle('random DSS 3D')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.set_ylim([0,110])
ax.grid(color='0.75', linestyle='--', linewidth=1)
#drawSampleInfluence(ax, x, y1, "Step 1")
#drawSampleInfluence(ax, x, y2, "Step 2")
#drawSampleInfluence(ax, x, y3, "Step 3")
#drawSampleInfluence(ax, x, y4, "Step 4")
drawSampleInfluence(ax, x, y5, "Step 5")
#drawSampleInfluence(ax, x, y6, "Step 6")
#drawSampleInfluence(ax, x, y7, "Step 7")
drawSampleInfluence(ax, x, y8, "Step 8")
#drawSampleInfluence(ax, x, y9, "Step 9")
#drawSampleInfluence(ax, x, y10, "Step 10")
drawSampleInfluence(ax, x, y11, "Step 11")
#drawSampleInfluence(ax, x, y12, "Step 12")
#drawSampleInfluence(ax, x, y13, "Step 13")
drawSampleInfluence(ax, x, y14, "Step 14")
#drawSampleInfluence(ax, x, y15, "Step 15")
#drawSampleInfluence(ax, x, y16, "Step 16")
drawSampleInfluence(ax, x, y17, "Step 17")
#drawSampleInfluence(ax, x, y18, "Step 18")
#drawSampleInfluence(ax, x, y19, "Step 19")
drawSampleInfluence(ax, x, y20, "Step 20")
plt.show()

y_avg2 = (y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9 + y10 + y11 + y12 + y13 + y14 + y15 + y16 + y17 + y18 + y19 + y20) / 20

# Plot
fig = plt.figure()
#fig.suptitle('Average DSS 3D')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.set_ylim([0,110])
ax.grid(color='0.75', linestyle='--', linewidth=1)
drawSampleInfluence(ax, x, y_avg1, "Average - grid")
drawSampleInfluence(ax, x, y_avg2, "Average - random")
plt.show()
