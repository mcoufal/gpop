#!/usr/bin/env bash
#------------------------------------------------------------------------------#
# Gaussian Processes based OPtimiser (GPOP) EXPERIMENTS in 4D                  #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Test effectivity of GPOP optimisation with and without automatic#
# parameter tuning.                                                            #
#------------------------------------------------------------------------------#

# TO BE SET: Experiment settings
EXPERIMENT_CLASS="${EXPERIMENT_CLASS:-optimisers}"
EXPERIMENTS_PER_SECTOR="${EXPERIMENTS_PER_SECTOR:-1}"
OPTIMISERS="${OPTIMISERS:-wo_mle mle_wo_R mle_w_R}"
SPECIALIZATION="${SPECIALIZATION:-mle}"

# TO BE SET: GP optimiser settings
ROUNDS="${ROUNDS:-25}"
KERNEL="${KERNEL:-KernelRBF}"
SAMPLES="${SAMPLES:-5}"
DSS="${DSS:-random}"
AF="${AF:-ExpectedImprovement}"
R="${R:-0.01}"
DELTA="${DELTA:-1.0}"
AUTOTUNE=${AUTOTUNE:-false}

# TO BE SET: Benchmmark settings
BENCHMARK="${BENCHMARK:-FnSphere}"
F_OPT="${F_OPT:-3}"
DIM1_MIN="${DIM1_MIN:-0}"
DIM1_MAX="${DIM1_MAX:-5}"
DIM2_MIN="${DIM2_MIN:-0}"
DIM2_MAX="${DIM2_MAX:-5}"
DIM3_MIN="${DIM3_MIN:-0}"
DIM3_MAX="${DIM3_MAX:-5}"
DIM4_MIN="${DIM4_MIN:-0}"
DIM4_MAX="${DIM4_MAX:-5}"
DIMENSIONS="${DIM1_MIN}:${DIM1_MAX} ${DIM2_MIN}:${DIM2_MAX} ${DIM3_MIN}:${DIM3_MAX} ${DIM4_MIN}:${DIM4_MAX}"

# initialisation
NUM_FILES=0
EXPERIMENT_SUBCLASS="${SPECIALIZATION}_kern-${KERNEL}_dss-${DSS}_af-${AF}_bench-${BENCHMARK}"
DIM_TEXT="4D"


# Run GPOP optimiser with settings specified by global variables:
# ROUNDS:     number of optimisation rounds
# KERNEL:     used kernel in GP
# SAMPLES:    number of samples used in DSS for each dimension
# R:          GP parameter (uncertainty of measurement)
# DELTA:      GP parameter (measure of dependency)
# DSS:        used domain space search strategy
# AF:         used acquisition function
# SEED:       used random seed for 'random' in Python (useful for creating same environment in experiments)
# BENCHMARK:  used benchmark function
# X_OPT:      optimal benchmark function input
# F_OPT:      optimal benchmark function value
# DIMENSIONS: domain size for each dimension in format MIN:MAX
function run_gp_optimiser_wo_mle {
    # run GP benchmark optimisation without MLE
    ../gpop.py \
    --rounds $ROUNDS \
    --gaussian-process $KERNEL $SAMPLES $R $DELTA \
    --domain-search-strategy $DSS \
    --acquisition-function $AF \
    --seed $SEED \
    --benchmark $BENCHMARK "${X_OPT}" $F_OPT $DIMENSIONS | tee -a ${OUTPUT_FOLDER}/experiment_${rand_i}_${rand_j}_${rand_k}_${rand_l}.log
}

function run_gp_optimiser_mle_wo_R {
    # run GP benchmark optimisation with MLE (without R)
    ../gpop.py \
    --rounds $ROUNDS \
    --gaussian-process $KERNEL $SAMPLES $R $DELTA \
    --autotune \
    --domain-search-strategy $DSS \
    --acquisition-function $AF \
    --seed $SEED \
    --benchmark $BENCHMARK "${X_OPT}" $F_OPT $DIMENSIONS | tee -a ${OUTPUT_FOLDER}/experiment_${rand_i}_${rand_j}_${rand_k}_${rand_l}.log
}

function run_gp_optimiser_mle_w_R {
    # run GP benchmark optimisation with MLE (with R)
    ../gpop.py \
    --rounds $ROUNDS \
    --gaussian-process $KERNEL $SAMPLES $R $DELTA \
    --autotune \
    --autotune-uncertainty \
    --domain-search-strategy $DSS \
    --acquisition-function $AF \
    --seed $SEED \
    --benchmark $BENCHMARK "${X_OPT}" $F_OPT $DIMENSIONS | tee -a ${OUTPUT_FOLDER}/experiment_${rand_i}_${rand_j}_${rand_k}_${rand_l}.log
}

function run_optimiser {

    if [ ${OPTIMISER} == "wo_mle" ]; then
        run_gp_optimiser_wo_mle
    elif [ ${OPTIMISER} == "mle_wo_R" ]; then
        run_gp_optimiser_mle_wo_R
    elif [ ${OPTIMISER} == "mle_w_R" ]; then
        run_gp_optimiser_mle_w_R
    else
        echo "Something went wrong!"
        exit 1
    fi

}

# Simulate GPOP optimiser run by printing run parameters to stdandard output.
# DEBUGGING PURPOSES
function fake_optimiser_run {
    # print benchmark optimisation information
    echo "---------------------------------------------------------------------"
    echo "FAKE ${OPTIMISER} optimiser run..."
}

# Log experiment settings.
function log_experiment_settings {
    timestamp=`date`
    echo "---------- $timestamp ----------"
    echo "Experiment settings:"
    echo " * EXPERIMENT_CLASS=$EXPERIMENT_CLASS"
    echo " * EXPERIMENT_SUBCLASS=${EXPERIMENT_SUBCLASS}"
    echo " * OPTIMISERS=${OPTIMISERS}"
    echo " * EXPERIMENTS_PER_SECTOR=$EXPERIMENTS_PER_SECTOR"
    echo "GP settings:"
    echo " * ROUNDS=${ROUNDS}"
    echo " * KERNEL=${KERNEL}"
    echo " * SAMPLES=${SAMPLES}"
    echo " * DSS=${DSS}"
    echo " * AF=${AF}"
    echo " * R=${R}"
    echo " * DELTA=${DELTA}"
    echo " * AUTOTUNE=${AUTOTUNE}"
    echo "Benchmark settings:"
    echo " * BENCHMARK=${BENCHMARK}"
    echo " * F_OPT=${F_OPT}"
    echo " * DIMENSIONS=${DIMENSIONS}"
}

# Log run settings.
function log_run_settings {
    echo "[INFO] Running experiment with optimiser '$OPTIMISER' and benchmark '$BENCHMARK' with optimum '[$X_OPT]' and optimal value '${F_OPT}'..."
}

# Collect cumulative minumim results in OUTPUT_FOLDER.
function collect_cm_results {
    # for each line in file
    echo "[INFO] Collecting results in $OUTPUT_FOLDER..."
    for row in `seq 1 ${ROUNDS}`; do
        # for each file
        sum=0
        unset max
        unset min
        for file in ${OUTPUT_FOLDER}/*.log; do
            # save sum(average), max, min
            num=`sed -n "${row}p" $file`
            sum=`echo "$sum + $num" | bc -l`
            if [ -z "$max" ] || (( $(echo "$num > $max" |bc -l) )); then
                max=$num
            fi
            if [ -z "$min" ] || (( $(echo "$min > $num" |bc -l) )); then
                min=$num
            fi
        done
        # proper rounding
        average=`echo "scale=10; $sum / $NUM_FILES" | bc -l`

        # calculate deviation
        average=`echo "$average - $F_OPT" | bc`
        max=`echo "$max - $F_OPT" | bc`
        min=`echo "$min - $F_OPT" | bc`

        # save results to files
        echo "  $row: [deviation] average: $average, max: $max, min: $min"
        echo $average >> ${OUTPUT_FOLDER}/avg.txt
        echo $max >> ${OUTPUT_FOLDER}/max.txt
        echo $min >> ${OUTPUT_FOLDER}/min.txt
    done
}


# --RUN EXPERIMENTS--

mkdir -p ./data/${EXPERIMENT_CLASS}/${EXPERIMENT_SUBCLASS}
log_experiment_settings | tee -a ./data/${EXPERIMENT_CLASS}/${EXPERIMENT_SUBCLASS}/log.txt
# create randomized optimum within bounds
for i in `seq ${DIM1_MIN} $((DIM1_MAX-1))`; do # 1D
    for j in `seq ${DIM2_MIN} $((DIM2_MAX-1))`; do # 2D
        for k in `seq ${DIM3_MIN} $((DIM3_MAX-1))`; do # 3D
            for l in `seq ${DIM4_MIN} $((DIM4_MAX-1))`; do # 4D
                for m in `seq 1 ${EXPERIMENTS_PER_SECTOR}`; do # per each sector
                    # generate random optimal 4D vector from $i, $j, $k and $l
                    tmp_i=`printf '0.%04d\n' $RANDOM`
                    rand_i=`echo "$tmp_i + $i" | bc`
                    tmp_j=`printf '0.%04d\n' $RANDOM`
                    rand_j=`echo "$tmp_j + $j" | bc`
                    tmp_k=`printf '0.%04d\n' $RANDOM`
                    rand_k=`echo "$tmp_k + $k" | bc`
                    tmp_l=`printf '0.%04d\n' $RANDOM`
                    rand_l=`echo "$tmp_l + $l" | bc`
                    X_OPT="$rand_i $rand_j $rand_k $rand_l" # set optimum
                    # Select 'random' SEED for the run
                    SEED=`echo $((1 + RANDOM % 999))`

                    # run optimisation for all optimisers
                    for OPTIMISER in ${OPTIMISERS}; do
                        EXPERIMENT_NAME="${OPTIMISER}_${DIM_TEXT}_r${ROUNDS}s${SAMPLES}"
                        OUTPUT_FOLDER="./data/${EXPERIMENT_CLASS}/${EXPERIMENT_SUBCLASS}/${EXPERIMENT_NAME}"
                        mkdir -p $OUTPUT_FOLDER
                        log_run_settings | tee -a ${OUTPUT_FOLDER}/log.txt
                        run_optimiser
                    done

                    (( NUM_FILES = NUM_FILES + 1 ))
                done
            done
        done
    done
done
echo "[INFO] Number of conducted experiments per one aquisition function: $NUM_FILES" | tee -a ./data/${EXPERIMENT_CLASS}/${EXPERIMENT_SUBCLASS}/log.txt


# --COLLECT RESULTS (cumulative minimum)--

for OPTIMISER in ${OPTIMISERS}; do
    EXPERIMENT_NAME="${OPTIMISER}_${DIM_TEXT}_r${ROUNDS}s${SAMPLES}"
    OUTPUT_FOLDER="./data/${EXPERIMENT_CLASS}/${EXPERIMENT_SUBCLASS}/${EXPERIMENT_NAME}"
    collect_cm_results
done
