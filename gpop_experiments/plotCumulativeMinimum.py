#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# Plot cumulative minimum                                                      #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Plot cumulative minimum                                         #
#------------------------------------------------------------------------------#

import os
import numpy as np
import argparse
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
import sys


def drawCumulativeMinimum(ax, rounds, y, label_name, colorMin="b", colorAct="r", linestyle="-"):
    best_y = y[0] + 1
    best_all = []
    steps_plot = list(range(1, len(y) + 1))
    steps = list(range(len(y)))
    for j in steps:
        # plot value of each step
        ax.plot(j + 1, y[j], c=colorAct, marker="o")
        if (y[j] <= best_y):
            best_y = y[j]
            best_all.append(y[j])
        else:
            best_all.append(best_y)
    # plot cumulative minimum
    ax.plot(steps_plot, best_all, c=colorMin, label=label_name, linestyle=linestyle)
    ax.legend()


def main():

    # get optimisation settings from args
    parser = argparse.ArgumentParser(description="Plot cumulative minimum.")

    # files options (paths to files with cumulative minimum values)
    parser.add_argument("dir", nargs='+')
    parser.add_argument("--avg", action="store_true")
    parser.add_argument("--min", action="store_true")
    parser.add_argument("--max", action="store_true")
    args = parser.parse_args()

    if not args.avg and not args.min and not args.max:
        print("At least one file has to be specified!")
        exit(1)

    # plot colours
    number = len(args.dir)
    cmap = plt.get_cmap('gnuplot')
    colours = [cmap(i) for i in np.linspace(0, 1, number)]
    colours = ["k", "b", "r", "g"]

    # font size
    plt.rcParams.update({'font.size': 30})

    # create plot
    fig = plt.figure(figsize=(16, 16))
    plt.xlabel('step')
    plt.ylabel('error')
    ax = fig.add_subplot(111)
    # X-axis: show only integers
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_facecolor('w')
    ax.set_ylim([0,110])
    ax.grid(color='0.75', linestyle='--', linewidth=1)

    file_num = 0
    for dir in args.dir:

        # plot averages
        if args.avg:
            file = "{}/avg.txt".format(dir)
            with open(file) as fp:
                min_list = []
                rounds = 0
                for line in fp:
                   min_list.append(float(line))
                   rounds += 1
            drawCumulativeMinimum(ax, rounds, min_list, file, colorMin=colours[file_num], colorAct=colours[file_num])

        # plot minimums
        if args.min:
            file = "{}/min.txt".format(dir)
            with open(file) as fp:
                min_list = []
                rounds = 0
                for line in fp:
                   min_list.append(float(line))
                   rounds += 1
            drawCumulativeMinimum(ax, rounds, min_list, file, colorMin=colours[file_num], colorAct=colours[file_num], linestyle=':')

        # plot maximums
        if args.max:
            file = "{}/max.txt".format(dir)
            with open(file) as fp:
                min_list = []
                rounds = 0
                for line in fp:
                   min_list.append(float(line))
                   rounds += 1
            drawCumulativeMinimum(ax, rounds, min_list, file, colorMin=colours[file_num], colorAct=colours[file_num], linestyle='--')

        file_num += 1

    plt.show()

if __name__== "__main__":
    main()
