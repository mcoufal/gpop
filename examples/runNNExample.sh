#!/usr/bin/env bash
# Example of running GPOP omtimizer on a neural network.
# Run `./gpop.py -h` to see parameter description.

 ../gpop/wrapper.py \
 --rounds 10 \
 --gaussian-process "KernelRBF" 10 0.00000001 0.5 \
 --config-file mnist_example_config.txt \
 --draw \
 --verbose
