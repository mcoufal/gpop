#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# GPOP - Examples                                                              #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Examples of using GPOP toolkit to optimise benchmark functions. #
#------------------------------------------------------------------------------#

import torch
from gpop.optimiser import optimiser as gpo
from gpop.optimiser import kernel
from gpop.optimiser import benchmarks

# benchmark settings
benchmark_name = "FnSphere"
benchmark_optimum = [2.0, 5.0]
benchmark_min_value = 2

# optimisation settings
num_rounds = 10
kernel_name = "KernelMaternARD52"
params = torch.tensor([1.0, 1.0, 1.0], requires_grad=True)
dim_bounds = [[0.0, 5.0], [0.0, 5.0]]

# create benchmark
fn = benchmarks.selectBenchmark(benchmark_name, benchmark_optimum, benchmark_min_value)

# select kernel
k = kernel.selectKernel(kernel_name, params)

# create optimiser
opt = gpo.GPOptimiser(fn, dim_bounds, k, autotune=True, verbose=True)

# run optimisation
opt.optimise(num_rounds)

# print results
print("Best settings found: hp1: {}, hp2: {}".format(opt.best_x[0], opt.best_x[1]))
print("Best function value found: {}".format(opt.best_y))
