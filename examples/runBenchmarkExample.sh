#!/usr/bin/env bash
# Example of running GPOP omtimizer on a benchmark function.
# Run `./gpop.py -h` to see parameter description.

 ../gpop/wrapper.py \
 --rounds 10 \
 --gaussian-process "KernelRBF" 10 0.01 1.0 \
 --acquisition-function "LowerConfidence" \
 --benchmark "FnEllipsoidal" "5" 3 "0:10" \
 --draw \
 --verbose
