#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# GPOP - Examples                                                              #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Examples of using GPOP toolkit to optimise MNIST NN.            #
#------------------------------------------------------------------------------#

import torch
from gpop.optimiser import optimiser as gpo
from gpop.optimiser import kernel
from gpop.nnbridge import bridge

# NN settings
config_path = "./mnist_example_config.txt"
neural_net, dim_bounds = bridge.parseFileConfig(config_path)

# optimisation settings
num_rounds = 5
kernel_name = "KernelRBF"
params = torch.tensor([1.0], requires_grad=True)

# select kernel
k = kernel.selectKernel(kernel_name, params)

# create optimiser
opt = gpo.GPOptimiser(neural_net, dim_bounds, k, verbose=True)

# run optimisation
opt.optimise(num_rounds)

# print results
print("Best settings found: {}: {}, {}: {}".format(neural_net.hparams[0].name, opt.best_x[0], neural_net.hparams[1].name, opt.best_x[1]))
print("Best loss found: {}".format(opt.best_y))
