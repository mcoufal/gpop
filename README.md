# Gaussian Processes based OPtimiser (GPOP)
This is my masters thesis project at Brno University of Technology - Faculty of Information Technology.

## What is it?
A Python toolkit for hyper-optimisation of neural networks. Implemented hyper-optimisation methods:
* grid search
* random search
* Bayesian optimisation (Gaussian processes)

## How to use it?
1. **Getting the toolkit**
 * clone this repository
 * go to the cloned repository: '`cd gpop`'
 * install the toolkit: '`python3 setup.py install`'
2. **Running the optimisation**
 * to run optimisation from command line, run CLI wrapper '`gpop/wrapper.py`' (see '`examples/`': '`runBenchmarkExample.sh`', '`runNNExample.sh`')
 * to use the toolkit from your Python code, simply import necessary modules from '`gpop`' (see '`examples/`': '`optimise_benchmark.py`', '`optimise_nn.py`')

**Note:** _For more details about the toolkit, see my [thesis](https://gitlab.com/mcoufal/dp)_
