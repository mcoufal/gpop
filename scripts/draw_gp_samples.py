#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# Draw kernel samples in 1D                                                    #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Draw samples from GP.                                           #
#------------------------------------------------------------------------------#

import numpy as np
import torch
from matplotlib import pyplot as plt
from gpop.optimiser import kernel
from gpop.optimiser import benchmarks


# Generate @n_fn_samples random samples from given kernel @kern, using training
# points @x_train and test_fn().
def generateRandomSamples(n_fn_samples, x_train, y_train, x_test, kern):
    n_test_samples = len(x_test)
    # apply kernel to testing points
    K_ss = getCov(x_test, x_test, kern)
    # apply kernel to training points
    K = getCov(x_train, x_train, kern)
    L = np.linalg.cholesky(K + 0.00005 * np.eye(len(x_train)))

    # compute the mean at our test points
    K_s = getCov(x_train, x_test, kern)
    Lk = np.linalg.solve(L, K_s)
    mu = np.dot(Lk.T, np.linalg.solve(L, y_train)).reshape((n_test_samples,))

    # compute standard deviation so we can plot it
    s2 = np.diag(K_ss) - np.sum(Lk**2, axis=0)
    stdv = np.sqrt(s2)

    # create @n_samples from the posterior at our test points.
    L = np.linalg.cholesky(K_ss + 1e-6*np.eye(n_test_samples) - np.dot(Lk.T, Lk))
    f_post = mu.reshape(-1,1) + np.dot(L, np.random.normal(size=(n_test_samples,n_fn_samples)))

    return f_post, mu, stdv

# Return covariance matrix for arrays @a and @b. Covariance is calculated using
# @kern function.
def getCov(a,b, kern):
    size_a = len(a)
    size_b = len(b)
    res = np.zeros((size_a, size_b))
    for i in range(size_a):
        for j in range(size_b):
            res[i][j] = kern(a[i], b[j])
    return res

# Example function for creating training points.
def test_fn(x):
    return (x - 2.5) ** 2 - torch.sin(x)

# Draw generated random kernel samples given by @x_test and @f_post.
def drawKernelSamples(x_test, f, x_train, y_train, mu, stdv, name, extras=False):
    # draw random samples
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_facecolor('w')
    ax.grid(color='0.75', linestyle='--', linewidth=1)
    # plot test fn
    f_x = []
    for val in x_test:
        f_x.append(f(val[0]))
    plt.plot(x_test, f_x, color='darkorange', linestyle='--')
    if extras:
        # draw uncertainty
        plt.gca().fill_between(x_test.flat, mu-2*stdv, mu+2*stdv, color="#dddddd", alpha=0.5)
        # draw mean
        plt.plot(x_test, mu, 'b')
    plt.axis([0, 5, -3, 3])
    plt.show()
    fig.savefig("../gpop_experiments/data/kernel_samples/{}_sample.pdf".format(name), bbox_inches='tight')


#------------------------------------------------------------------------------#
#                                    Main                                      #
#------------------------------------------------------------------------------#
def main():
    # general settings
    n_test_samples = 500
    n_fn_samples = 3
    tested_fn = benchmarks.selectBenchmark("FnSphere", 2, 3)

    # testing points
    x_test = torch.from_numpy(np.linspace(0, 5, n_test_samples).reshape(-1,1))

    # TRAINING: none
    x = torch.from_numpy(np.array([]).reshape(0,1))
    y = test_fn(x)

    # NO SAMPLES
    params = torch.from_numpy(np.array([0.1]))
    paramsMatern = torch.from_numpy(np.array([1.0, 0.1]))
    kern = kernel.KernelRBF(params)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test.numpy(), tested_fn, x, y, mu, stdv, "prior", extras=True)


if __name__== "__main__":
    main()
