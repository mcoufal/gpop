#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# Draw 1D and 2D benchmark samples.                                            #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Draw benchmark samples from benchmarks module.                  #
#------------------------------------------------------------------------------#

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import Axes3D
import itertools
from gpop.optimiser import benchmarks


def drawPrediction2D(ax, x, fn, alpha, optimum):
    tmp_y = []
    tmp_x = x.T

    for i in range(0, len(tmp_x)):
        next_y = fn(tmp_x[i])
        tmp_y.append(next_y)

    ax.set_facecolor('w')
    ax.plot(x[0], tmp_y, alpha=alpha, zorder=1)
    ax.scatter(optimum, fn(optimum), marker='x', color='r', s=50, zorder=2)

    ax.set_xlabel('x_1', fontsize=22)
    ax.set_ylabel('x_2', fontsize=22, labelpad=10)
    ax.tick_params(axis='both', which='major', labelsize=18)
    ax.tick_params(axis='both', which='minor', labelsize=18)

def drawPrediction3D(ax, x, fn, alpha, cmap, optimum):
    tmp_y = []
    tmp_x = x.T
    # get Z coordinates for mean prediction and plot it
    for i in range(0, len(tmp_x)):
        next_y = (fn(tmp_x[i]))
        tmp_y.append(next_y)

    # set axis to equal scale
    tmp_y = np.array(tmp_y)
    max_range = np.array([x[0].max()-x[0].min(), x[1].max()-x[1].min(), tmp_y.max()-tmp_y.min()]).max() / 2.0
    mid_x = (x[0].max()+x[0].min()) * 0.5
    mid_y = (x[1].max()+x[1].min()) * 0.5
    mid_z = (tmp_y.max()+tmp_y.min()) * 0.5
    ax.set_xlim(0, 5)
    ax.set_ylim(0, 5)
    ax.set_zlim(tmp_y.min(), tmp_y.max())

    ax.plot_trisurf(x[0], x[1], tmp_y, alpha=1, cmap=cmap, linewidth=0, antialiased=True)
    #ax.scatter(optimum[0], optimum[1], fn(optimum), marker='x', color='r', s=50)
    ax.quiver(
        optimum[0], optimum[1], fn(optimum) - 1,
        0, 0, 0.9,
        color = 'red', alpha = 1, lw = 2,
    )

    # name axes
    ax.set_xlabel('x_1', fontsize=22, labelpad=20)
    ax.set_ylabel('x_2', fontsize=22, labelpad=20)
    ax.set_zlabel('f(x)', fontsize=22, labelpad=10)
    ax.set_facecolor('w')
    ax.tick_params(axis='both', which='major', labelsize=18)
    ax.tick_params(axis='both', which='minor', labelsize=18)

# TODO: text size
def drawContour3D(ax, x, y, fn, alpha, cmap):
    Z = []
    X, Y = np.meshgrid(x, y)
    # function fn() take args as array, so we need to compute it for each
    # point of the mesh separately
    for i in range (len(X)):
        tmp_z = []
        for j in range(len(X[i])):
            tmp = np.array([X[i][j], Y[i][j]])
            tmp_z.append(fn(tmp))
        Z.append(tmp_z)
    ax.set_facecolor('w')
    ax.contour(X, Y, np.array(Z), 20, cmap=cmap)
    ax.scatter(2.5, 2.5, marker='x', color='r', s=50, zorder=2)

    ax.set_xlabel('x_1', fontsize=30)
    ax.set_ylabel('x_2', fontsize=30, labelpad=10)
    ax.tick_params(axis='both', which='major', labelsize=24)
    ax.tick_params(axis='both', which='minor', labelsize=24)

def generatePlotXCoordinates(dim_bounds, nb_samples):
    # create list of linear spaced arrays, each containing values of one dimension
    x_tmp = []
    for i in range(0,len(dim_bounds)):
        x_tmp.append(np.linspace(dim_bounds[i][0], dim_bounds[i][1], nb_samples))

    # create cartesian product of those arrays and convert it into matrix
    cartesian_tuples = [item for item in itertools.product(*x_tmp)]
    cartesian_list = [list(ele) for ele in cartesian_tuples]
    x_guess = np.array(cartesian_list)

    return x_guess.transpose()


#------------------------------------------------------------------------------#
#                                    Main                                      #
#------------------------------------------------------------------------------#
def main():
    # Global settings
    draw_samples = 50

    #--------------------------------------------------------------------------#
    # SPHERE

    # 1D plot

    # benchmark settings
    benchmark_name = "FnSphere"
    benchmark_optimum = [2.5]
    benchmark_min_value = 2
    dim_bounds = [[0, 5]]
    # create benchmark
    fn = benchmarks.selectBenchmark(benchmark_name, benchmark_optimum, benchmark_min_value)
    # draw
    x = generatePlotXCoordinates(dim_bounds, draw_samples)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(color='0.75', linestyle='--', linewidth=1)
    drawPrediction2D(ax, x, fn, 1, np.array(benchmark_optimum))
    plt.show()

    # 2D plot

    # benchmark settings
    benchmark_name = "FnSphere"
    benchmark_optimum = [2.5, 2.5]
    benchmark_min_value = 2
    dim_bounds = [[0, 5], [0, 5]]
    # create benchmark
    fn = benchmarks.selectBenchmark(benchmark_name, benchmark_optimum, benchmark_min_value)
    # draw
    x = generatePlotXCoordinates(dim_bounds, draw_samples)
    fig = plt.figure(figsize=(9,7))
    ax = fig.add_subplot(111, projection='3d')
    drawPrediction3D(ax, x, fn, 1, cm.coolwarm, np.array(benchmark_optimum))
    plt.show()

    # 2D contour

    x = np.linspace(dim_bounds[0][0], dim_bounds[0][1], draw_samples)
    y = np.linspace(dim_bounds[1][0], dim_bounds[1][1], draw_samples)
    fig = plt.figure(figsize=(9,9))
    ax = fig.add_subplot(111)
    drawContour3D(ax, x, y, fn, 1, cm.coolwarm)
    plt.show()

    #--------------------------------------------------------------------------#
    # ELLIPSOIDAL

    # 1D plot

    # benchmark settings
    benchmark_name = "FnEllipsoidal"
    benchmark_optimum = [2.5]
    benchmark_min_value = 2
    dim_bounds = [[0, 5]]
    # create benchmark
    fn = benchmarks.selectBenchmark(benchmark_name, benchmark_optimum, benchmark_min_value)
    # draw
    x = generatePlotXCoordinates(dim_bounds, draw_samples)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(color='0.75', linestyle='--', linewidth=1)
    drawPrediction2D(ax, x, fn, 1, np.array(benchmark_optimum))
    plt.show()

    # 2D plot

    # benchmark settings
    benchmark_name = "FnEllipsoidal"
    benchmark_optimum = [2.5, 2.5]
    benchmark_min_value = 2
    dim_bounds = [[0, 5], [0, 5]]
    # create benchmark
    fn = benchmarks.selectBenchmark(benchmark_name, benchmark_optimum, benchmark_min_value)
    # draw
    x = generatePlotXCoordinates(dim_bounds, draw_samples)
    fig = plt.figure(figsize=(9,7))
    ax = fig.add_subplot(111, projection='3d')
    drawPrediction3D(ax, x, fn, 1, cm.coolwarm, np.array(benchmark_optimum))
    plt.show()

    # 2D contour

    x = np.linspace(dim_bounds[0][0], dim_bounds[0][1], draw_samples)
    y = np.linspace(dim_bounds[1][0], dim_bounds[1][1], draw_samples)
    fig = plt.figure(figsize=(9,9))
    ax = fig.add_subplot(111)
    drawContour3D(ax, x, y, fn, 1, cm.coolwarm)
    plt.show()



if __name__== "__main__":
    main()
