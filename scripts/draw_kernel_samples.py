#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# Draw kernel samples in 1D                                                    #
# Author: Martin Coufal                                                        #
# Email: xmcoufal@gmail.com                                                    #
#------------------------------------------------------------------------------#
# Description: Draw samples from kernels defined in optimiser.kernel in 1D.    #
#------------------------------------------------------------------------------#

import numpy as np
import torch
from matplotlib import pyplot as plt
import seaborn as sns
from optimiser import kernel
sns.set(color_codes=True)


# Generate @n_fn_samples random samples from given kernel @kern, using training
# points @x_train and test_fn().
def generateRandomSamples(n_fn_samples, x_train, y_train, x_test, kern):
    n_test_samples = len(x_test)
    # apply kernel to testing points
    K_ss = getCov(x_test, x_test, kern)
    # apply kernel to training points
    K = getCov(x_train, x_train, kern)
    L = np.linalg.cholesky(K + 0.00005 * np.eye(len(x_train)))

    # compute the mean at our test points
    K_s = getCov(x_train, x_test, kern)
    Lk = np.linalg.solve(L, K_s)
    mu = np.dot(Lk.T, np.linalg.solve(L, y_train)).reshape((n_test_samples,))

    # compute standard deviation so we can plot it
    s2 = np.diag(K_ss) - np.sum(Lk**2, axis=0)
    stdv = np.sqrt(s2)

    # create @n_samples from the posterior at our test points.
    L = np.linalg.cholesky(K_ss + 1e-6*np.eye(n_test_samples) - np.dot(Lk.T, Lk))
    f_post = mu.reshape(-1,1) + np.dot(L, np.random.normal(size=(n_test_samples,n_fn_samples)))

    return f_post, mu, stdv

# Return covariance matrix for arrays @a and @b. Covariance is calculated using
# @kern function.
def getCov(a,b, kern):
    size_a = len(a)
    size_b = len(b)
    res = np.zeros((size_a, size_b))
    for i in range(size_a):
        for j in range(size_b):
            res[i][j] = kern(a[i], b[j])
    return res

# Example function for creating training points.
def test_fn(x):
    return (x - 2.5) ** 2 - torch.sin(x)

# Draw generated random kernel samples given by @x_test and @f_post.
def drawKernelSamples(x_test, f_post, x_train, y_train, mu, stdv, name, extras=False):
    # draw random samples
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_facecolor('w')
    ax.grid(color='0.75', linestyle='--', linewidth=1)
    # draw kernel random samples
    plt.plot(x_test, f_post)
    if extras:
        # draw uncertainty
        plt.gca().fill_between(x_test.numpy().flat, mu-2*stdv, mu+2*stdv, color="#dddddd", alpha=0.5)
        # draw mean
        plt.plot(x_test, mu, 'k--', lw=1)
    # draw training points
    plt.plot(x_train, y_train, "o", color="black")
    plt.axis([0, 5, -3, 3])
    plt.show(block=False)
    fig.savefig("gpop_experiments/data/kernel_samples/{}_sample.pdf".format(name), bbox_inches='tight')


#------------------------------------------------------------------------------#
#                                    Main                                      #
#------------------------------------------------------------------------------#
def main():
    # general settings
    n_test_samples = 1000
    n_fn_samples = 3

    # testing points
    x_test = torch.from_numpy(np.linspace(0, 5, n_test_samples).reshape(-1,1))

    # TRAINING: none
    x = torch.from_numpy(np.array([]).reshape(0,1))
    y = test_fn(x)

    # NO SAMPLES
    params = torch.from_numpy(np.array([0.1]))
    paramsMatern = torch.from_numpy(np.array([1.0, 0.1]))
    kern = kernel.KernelRBF(params)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "prior")

    # RBF kernel without training points
    params = torch.from_numpy(np.array([0.1]))
    paramsMatern = torch.from_numpy(np.array([1.0, 0.1]))
    kern = kernel.KernelRBF(params)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "RBF_kernel")

    # Matern kernel without training points
    kern = kernel.KernelMaternARD52(paramsMatern)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "Matern_kernel")

    # Laplacian kernel without training points
    kern = kernel.KernelLaplacian(params)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "Laplacian_kernel")

    # RBF + Constant kernel without training points
    k1 = kernel.KernelRBF(params)
    k2 = kernel.KernelConstant(5.0)
    kern = kernel.buildAddKernel(k1, k2)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "RBF_Constant_add_kernel")

    # RBF * constant
    k1 = kernel.KernelRBF(params)
    k2 = kernel.KernelConstant(0.5)
    kern = kernel.buildMultKernel(k1, k2)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "RBF_Constant_mult_kernel", extras=True)

    # (RBF + constant) * constant
    k1 = kernel.KernelRBF(params)
    k2 = kernel.KernelConstant(5.0)
    k3 = kernel.KernelConstant(0.5)
    kernAdd = kernel.buildAddKernel(k1, k2)
    kern = kernel.buildMultKernel(kernAdd, k3)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "RBF_Constant_add_mult_kernel", extras=True)

    # TRAINING: 3 points
    x = torch.from_numpy(np.array([1.0, 2.0, 3.0]).reshape(3,1))
    y = test_fn(x)

    # RBF kernel with training points
    kern = kernel.KernelRBF(params)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "RBF_kernel_TR", extras=True)

    # Matern kernel with training points
    kern = kernel.KernelMaternARD52(paramsMatern)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "Matern_kernel_TR", extras=True)

    # Laplacian kernel with training points
    kern = kernel.KernelLaplacian(params)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "Laplacian_kernel_TR", extras=True)

    # RBF + Constant kernel with training points
    k1 = kernel.KernelRBF(params)
    k2 = kernel.KernelConstant(5.0)
    kern = kernel.buildAddKernel(k1, k2)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "RBF_Constant_kernel_add_TR", extras=True)

    # RBF * constant
    k1 = kernel.KernelRBF(params)
    k2 = kernel.KernelConstant(0.5)
    kern = kernel.buildMultKernel(k1, k2)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "RBF_Constant_mult_kernel_TR", extras=True)

    # (RBF + constant) * constant
    k1 = kernel.KernelRBF(params)
    k2 = kernel.KernelConstant(5.0)
    k3 = kernel.KernelConstant(0.5)
    kernAdd = kernel.buildAddKernel(k1, k2)
    kern = kernel.buildMultKernel(kernAdd, k3)
    f_post, mu, stdv = generateRandomSamples(n_fn_samples, x, y, x_test, kern)
    drawKernelSamples(x_test, f_post, x, y, mu, stdv, "RBF_Constant_add_mult_kernel_TR", extras=True)

if __name__== "__main__":
    main()
