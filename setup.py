from setuptools import setup

setup(
   name='gpop',
   version='1.0',
   description='Gaussian Process based Optimisation toolkit',
   author='Martin Coufal',
   author_email='xmcoufal@gmail.com',
   packages=['gpop', 'gpop.optimiser', 'gpop.nnbridge'],
   install_requires=['numpy', 'torch', 'matplotlib', 'seaborn']
)
